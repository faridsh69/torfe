<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','deleted_at','updated_at']; 
    use SoftDeletes;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function state()
    {
    	return $this->belongsTo('App\Models\State');
    }

    public function city()
    {
    	return $this->belongsTo('App\Models\City');
    }

    public function province()
    {
    	return $this->belongsTo('App\Models\Province');
    }
}
