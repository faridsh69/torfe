
@extends('layout.master')
@section('container')
<div class="seperate"></div>

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <h2 class="text-center page-header">
        معرفی مرکز نوآوری و کارآفرینی طرفه نگار
        </h2>
        <div class="seperate"></div>
        <h4 style="line-height: 30px;text-align: justify;">
شرکت طرفه‌ نگار از سال 1377 با هدف نهادینه کردن استفاده از تکنولوژی اطلاعات در روندهای شغلی، پا به عرصه طراحی و تولید نرم افزار نهاد. در سال 1394، مرکز نوآوری و کارآفرینی طرفه ‌نگار نیز در همین راستا و به منظور ایجاد بستر رشد و شکوفایی برای کمک به کسب و کارهای نوپایی که برنامه تولید محصول و یا ارائه خدمات را دارند؛ آغاز به فعالیت نمود و به عرصه‌ی نوآوری و کارآفرینی کشور ورود پیدا کرد. در طی این سال ها نیز با موفقیت و پیشرفت روزافزون به فعالیت خود ادامه داده و ماموریت ها و اهداف خود را تحقق بخشیده است.
        </h4>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12 text-center">
        <img src="/public/img/20.jpg" style="max-width: 100%" class="img-circle">
    </div>
</div>

<div class="row">
	<div class="col-xs-12 text-center">
		<h3>
			درباره ما: 
		</h3>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<p style="padding: 20px;line-height:30px;font-size: 130%;text-align: justify;letter-spacing: 0.5px;">
<!-- مرکز نوآوری و کارآفرینی طرفه نگار یک مجموعه نوآور و کارآفرین در حوزه  شتابدهی و  سرمایه گذاری و بر روی کسب و کارهای نوپا ( استارتاپ ها) می باشد که تا کنون با چندین استارتاپ در حوزه های سرمایه گذاری و خدمات شتاب دهی و مشاوره همکاری داشته است و همواره دست تیم ها و مجموعه های توانمندی که درصدد نیل به اهداف تعالی بخش خود می باشند را به گرمی می فشارد
 -->
خدمت اصلی ما اعتبارسنجی ایده های نوآورانه، و ارائه خدمات کارآفرینانه به منظور ایجاد یک کسب و کار واقعیست. کمک به تدوین طرح کسب و کار مناسب و تشکیل تیم کاری و اجرای ایده ها تا مرحله ای که کسب و کار آماده ارائه به سرمایه گذار یا ورود به بازار باشد .
<br>
همچنین با در نظر گرفتن ماهیت ایده و کسب و کار و توانمندی های بنیانگذاران، خدماتی چون تأمین محل استقرار و امکانات مرتبط، مشاوره تخصصی، جذب نیرو و تأمین هزینه های جاری کسب و کار نوپا ، ارائه ایده ، امکان ارتباط با بازار و مشتریان هلدینگ طرفه نگار ، معرفی به سرمایه گذار و ... تا یک مرحله مشخص از دوره عمر کسب و کار ارائه می دهد .
 
		</p>
	</div>
</div>
<div class="seperate"></div>
@endsection
