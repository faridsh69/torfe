<?php

use Illuminate\Database\Seeder;

class DetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $address = 
            ['id' => 1,'sabet' => '021-11223344', 'phone' => '09122163498','user_id' => 2, 
                'name' => 'چسبیده به استادیوم آزادی همون بغل دست راست'];
        
        \App\Models\Address::firstOrCreate($address); 

        $shop = 
            ['id' => 1,'name' => 'فروشگاه' ,'image_id' => 1, 'address_id' => 1, 'user_id' => 2, 
            'start_time' => '6', 'end_time' => '11', 'credit_card' => '1934321234123456', 
            'payk' => '2000', 'off' => '15', 'minimum_price' => '4000', 'email' => 
            'farid.sh69@gmail.com'];
        
        \App\Models\Shop::firstOrCreate($shop); 
    }
}
