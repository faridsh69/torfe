@extends('layout.print')
@section('container')
<div class="double-seperate hidden-print"></div>
<a class="btn btn-block btn-primary hidden-print" href="/admin/order/manage">بازگشت
<span class="glyphicon glyphicon-arrow-left"></span>  </a>
<div class="double-seperate hidden-print"></div>
<div class="text-center">
	سفارش از {{ \App\Http\Controllers\Controller::NAME }}
</div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-6">
	تاریخ:
	{{ \Nopaad\jDate::forge( $order->updated_at )->format(' %Y/%m/%d') }}
	</div>
	<div class="col-xs-6">
		<div style="float:left">
		ساعت:
		{{ \Nopaad\jDate::forge( $order->updated_at )->format(' %H:%M:%S') }}
		</div>
	</div>
</div>
<div class="half-seperate"></div>
<div class="row">
	<div class="col-xs-6">
	شماره سفارش: {{ $order->id }}
	</div>
	<div class="col-xs-6">
		<div style="float:left">
		
		</div>
	</div>
</div>
<div class="seperate"></div>
@if($order->user)
<h4> اطلاعات مشتری</h4>
<div class="half-seperate"></div>
<div class="row">
	<div class="col-xs-12">
	نام و نام‌خانوادگی:
		{{ $order->user->first_name }}
		{{ $order->user->last_name }}
	</div>
</div>
<div class="half-seperate"></div>
<div class="row">
	<div class="col-xs-12">
		شماره همراه مشتری: {{ $order->user->phone }}
	</div>
</div>
<div class="half-seperate"></div>
</div>
	<div class="col-xs-12">
		آدرس: {{ $order->address->name }}
	</div>
</div>
@else
<div class="alert alert-danger text-center">
	اطلاعات مشتری وارد نشده
</div>
@endif
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th>
				تعداد	
			</th>
			<th>
				نام غذا
			</th>
			<th>
				مبلغ
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($products->unique('name') as $product)
		<tr>
			<td>
				{{  \Nopaad\Persian::correct($products->where('name',$product->name)->count()) }}
			</td>
			<td>
				{{ $product->name }}
			</td>
			<td>
				{{  \Nopaad\Persian::correct( number_format(
				$products->where('name',$product->name)->count() * $product->price * (100 - \App\Http\Controllers\Controller::OFF  ) / 100
				, 0, '',',')) }}
			</td>
		</tr>
		@endforeach
		</tbody>
		</table>
	</div>
	<div class="col-sm-6">
		<div class="seperate"></div>
		<dl class="dl-horizontal">
		  	<dt>جمع کل هزینه ها</dt>
		  	<dd>{{  \Nopaad\Persian::correct(  number_format( $products->sum('price'), 0, '',',') ) }} تومان</dd>
		  	<div class="half-seperate"></div>
		  	<dt>جمع هزینه ها پس از تخفیف</dt>
		  	<dd>{{  \Nopaad\Persian::correct(  number_format($products->sum('price') * (100 - \App\Http\Controllers\Controller::OFF ) / 100 , 0, '',',')) }} تومان</dd>
		  	<div class="half-seperate"></div>
		  	<dt>هزینه ارسال</dt>
		  	<dd>{{ \Nopaad\Persian::correct( number_format( \App\Http\Controllers\Controller::PEYK  ), 0, '',',') }} تومان</dd>
		  	<div class="half-seperate"></div>
		  	<dt class="double-size">هزینه قابل پرداخت</dt>
		  	<dd class="double-size">{{ \Nopaad\Persian::correct(  number_format( $products->sum('price') * (100 - \App\Http\Controllers\Controller::OFF ) / 100 + \App\Http\Controllers\Controller::PEYK  , 0, '',',')) }} تومان</dd>
		  	<div class="seperate"></div>
		</dl>
	</div>
</div>
<script type="text/javascript">
	window.print();
</script>
@endsection