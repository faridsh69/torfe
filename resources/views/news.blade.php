
@extends('layout.master')
@section('container')
<div class="seperate"></div>

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <h2 class="text-center page-header">
        اخبار‌ مرکز کارآفرینی طرفه نگار 
        </h2>
        <div class="seperate"></div>
        </h4>
    </div>
</div>


<div class="row" style="background: white;">
    @foreach(\App\Models\News::get() as $news)
        <div class="col-xs-6">
            <div class="cart">
                <div class="media">
                    <div class="media-left" style="width: 25%">
                        @if($news->image)
                            <img src="/storage/news/{{ $news->image->id }}-{{ $news->image->name}}" 
                            style="width: 100%;">
                        @endif
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <small>{{ $news->top_title }}</small>   
                            <div class="half-seperate"></div>
                            {{ $news->title }}
                            <div class="one-third-seperate"></div>
                            <small>{{ $news->sub_title }}</small> 
                        </h4>
                        <div class="seperate"></div>
                        <p style="text-align: justify;">
                            {{ $news->body }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>

<div class="seperate"></div>
@endsection
