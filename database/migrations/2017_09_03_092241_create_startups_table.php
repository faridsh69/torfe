<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('tarikh')->nullable();
            $table->string('request')->nullable();
            $table->text('kholase')->nullable();
            $table->text('ravesh')->nullable();
            $table->text('aza')->nullable();
            $table->string('mahsolyaide')->nullable();
            $table->text('sarmaye')->nullable();
            $table->boolean('ready')->default(1);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startups');
    }
}
