<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapClient;
use App\Notifications\KharidKonande;
use App\Notifications\ForgetPassword;
use Jenssegers\Agent\Agent as Agent;


class MasterController extends Controller
{
    
    public function getBlog()
    {
        return view('blog');
    }
    
    public function getNews()
    {
        return view('news');
    }

    public function getMentor()
    {
        return view('mentor');
    }

    public function getAbout()
    {
        return view('about-us');
    }

    public function getHome() 
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            $mobile = true;
        } else {
            $mobile = false;
        }   
        return view('home')->withMobile($mobile);
    }

    public function getRegisterIdea()
    {
        \Meta::set('title','ثبت استارتاپ');

        return view('register-idea');
    }

    public function postRegisterIdea(Request $request)
    {
        if(!\Auth::id()){
            \Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required',
                'phone' => 'required|numeric|unique:users|digits:11',
            ])->validate();
            $user = \App\User::create([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'phone' => $request['phone'],
                'email' => $request['email'],
                'password' => bcrypt( strtolower($request['password']) ),
            ]);
            if (\Auth::attempt(['phone' => $request['phone'], 'password' => strtolower($request['password']) ],
                100000 )) {
                
            } 
        }
        $request->session()->flash('alert-success', 'ثبت اطلاعات شما با موفقیت انجام شد.');
        \Log::info('user register with phone: '. $request['phone']);

        sleep(0.2);
        \App\Models\Startup::firstOrCreate([
            'name' => $request['companyName'],
            'tarikh' => $request['tarikh'],
            'request' => $request['request'],
            'kholase' => $request['kholase'],
            'aza' => $request['aza'],
            'mahsolyaide' => $request['mahsolyaide'],
            'sarmaye' => $request['sarmaye'],
            'ravesh' => $request['ravesh'],            
            'user_id' => \Auth::id()
        ]);
        // $this->_sms_kharidkarde($request['phone']);
        return redirect()->intended('/');
    }

    public function getSearch($q)
    {
        // $output = \Cache::remember('product', 21600, function () {
            // return \App\Models\Product::select('id','name')->get();
        // });
        $i = 1 ;
        $products = \App\Models\Product::select('id','name')->get();
        if (strlen($q)>0) {
            $hint="";
            foreach($products as $product)
            {
                if (stristr($product['name'],$q)) {
                    if ($hint=="") {
                        $hint="<ul><li><a href='/product/". 
                        $product['id'] . 
                        "-" . 
                        $product['name'] . 
                        "' target='_self'>" . 
                        $product['name'] . "</a></li>";
                    } else {
                        $i ++ ;
                        if($i > 8){
                            return $hint;
                        }
                        $hint=$hint . "<li><a href='/product/" . 
                        $product['id'] . 
                        "-" . 
                        $product['name'] . 
                        "' target='_self'>" . 
                        $product['name'] . "</a></li>";
                    }
                }
            }
        }
        return $hint;
        $out = "<ul>
            <li><a href='/type/1'>ظروف یکبار مصرف یک</a></li>
            <li><a href='/type/2'>ظروف یکبار مصرف دو</a></li>
            <li><a href='/type/3'>ظروف یکبار مصرف سه</a></li>
            <li><a href='/type/4'>ظروف یکبار مصرف چهار</a></li>
        </ul>";
    }

    public function getSabadekharid()
    {   
        \Meta::set('title','آدرس دهی');

        if(\Auth::id())
        {
            $order = \App\Models\Order::where('user_id', \Auth::id() )
            ->whereIn('status',['select','address','checkout','failed'] )->first();
        } else {
            $order = \App\Models\Order::where('user_ip', \Requests::ip())->where('user_id', null)
                ->whereIn('status',['select','address','checkout','failed'])->first();
                
            // $order = \App\Models\Order::where('user_ip', \Requests::ip())->whereIn('status',
            // ['select','address','checkout','failed'] )->first();
        }
        if(!$order){
            \Log::warning('order not found with getAddress : ordder' );
            return redirect('/');
        }
        return redirect('/address/'.$order->id);
    }

    public function postPutAddress(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
        ])->validate();
        \App\Models\Address::firstOrCreate([
            'name' => $request['address'],
            'postal_code' => $request['postcode'],
            'phone' => $request['phone'],
            'sabet' => $request['sabet'],
            'reciever' => $request['name'],
            'user_id' => \Auth::id(),
            // 'province_id' => '',
            // 'city_id' => '',
            // 'state_id' => '',
        ]);
        return redirect()->back();
    }
    
    
    // public function _like_restaurant()
    // {
    //     \App\Models\LikeRestaurant::create([
    //         'restaurant_id' => $id,
    //         'user_id' => $user->id,
    //         'like' => 1
    //     ]);
    // }

    // dis like ham mikhaim besazim

    public function getPaymentWithCredit($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPaymentWithCredit : ordder_id ' . $id );
            return redirect('/');
        }
        if(\Auth::user()->credit > $order->price){
            $order->status = "paycredit";
            $order->save();
            $user = \Auth::user();
            $user->credit = $user->credit - $order->price;
            $user->save();
            \Log::info('با اعتبارش پرداخت می کند order_id: ' . $id . ' by user_id: '. \Auth::id() );
            return view('verify')->withCode(1)->withOrder($order);
        }else{
            return view('verify')->withCode(3)->withOrder($order);
        }
    }

    public function getPaymentInLocation($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPaymentInLocation : ordder_id ' . $id );
            return redirect('/');
        }
        if(\App\Models\Order::where('user_id',\Auth::user()->id )->where('status','receive')->count() > 0 )
        {
            $order = \App\Models\Order::where('id', $id)->where('address_id','!=','')->first();
            $order->status = "willpay";
            $order->save();
            \Log::info('در محل پرداخت می کند order_id: ' . $id . ' by user_id: '. \Auth::id() );
            return view('verify')->withCode(4)->withOrder($order);
        }else{
            return view('verify')->withCode(3)->withOrder($order);
        }
    }

    public function getVerify()
    {
        \Meta::set('title','بازگشت از بانک');
        // risky
        $payment = \App\Models\Payment::where('user_id', \Auth::id())->orderBy('id','dsc')->first();
        $order = $payment->order;
        $MerchantID = self::MERCHANT_CODE;
        $Amount = $order->price;
        $Authority = $_GET['Authority'];

        if ($_GET['Status'] == 'OK') {
            $client = new \SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl',array('encoding' => 'UTF-8')); 
            $result = $client->PaymentVerification(
                [
                'MerchantID' => $MerchantID,
                'Authority' => $Authority,
                'Amount'   => $Amount,
                ]
            );
            if ($result->Status == 100) {
                \Log::info('payment good done : by payment_id: '. $payment->id . ' invN : ' .$result->RefID);
                $code = 1;
                $order->status = 'paid';
                $payment->description = $result->RefID;
                $payment->status = 1;
                $payment->Invoice_number = (int)$Authority;
                
                // vase saheb restoran sms bezan
            } else {
                \Log::warning('payment has error : by payment_id: '. $payment->id . ' status : ' .$result->Status);
                $code = 2;
                $payment->Invoice_number = (int)$Authority;
                $payment->description = self::_getErrorZarrinpal($result->Status);
            }
        } else {
            \Log::warning('user canceled: by payment_id: '. $payment->id );
            $code = 3;
            $payment->Invoice_number = (int)$Authority;
            $payment->description = 'کاربر کنسل کرده';
        }
        $payment->save();
        $order->save();
        
        return view('verify')->withCode($code)->withOrder($order);
    }

    public function getPayment($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getPayment : ordder_id ' . $id );
            return redirect('/');
        }

        if($order->price < 1000){
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "قیمت اشتباه" رخ داده است. سریعا مشکل رفع می شود');
        }

        $payment = \App\Models\Payment::create([
            'user_id' => \Auth::id(),
            'order_id' => $id,
            'user_ip' => \Requests::ip(),
            'user_agent' => \Requests::header('User-Agent'),
            'amount' => $order->price,
            'Invoice_date' => date('now'),
            'description' => 'در حال ورود به بانک',
            'status' => 0,
            ]);
            \Log::info('در حال ورود به بانک با order_id: ' . $id . ' by user_id: '. \Auth::id() 
            . ' with payment_id: ' . $payment->id );
        
        $MerchantID = self::MERCHANT_CODE;           
        $Amount = $order->price; 
        $Description = 'سفارش از ' . self::NAME;  
        $Email = \Auth::user()->email; 
        $Mobile = \Auth::user()->phone; 
        $CallbackURL = url('verify'); 
          
        // URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
        $client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
          
        $result = $client->PaymentRequest(
            array(
                'MerchantID'   => $MerchantID,
                'Amount'   => $Amount,
                'Description'   => $Description,
                'Email'   => $Email,
                'Mobile'   => $Mobile,
                'CallbackURL'   => $CallbackURL
            )
        );
          
        //Redirect to URL You can do it also by creating a form
        if($result->Status == 100)
        {
            $payment->Invoice_number = $result->Authority;
            $payment->save();
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);exit;
        } else {
            $payment->description = 'اطلاعات بانک تو سیستم غلطه';
            $payment->save();
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',$result->Status);
            \Log::warning(' "اطلاعات بانک اشتباه" رخ داده است. ',' کد خطا: '.$result->Status );
        }
    }

    public function getCheckout($id)
    {
        \Meta::set('title','پرداخت');

        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getCheckout : ordder_id ' . $id );
            return redirect('/');
        }

        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);

        $price = 1;
        return view('checkout')->withOrder($order)->withProducts($products);
    }

    public function postAddress($id,Request $request)
    {
        \Validator::make($request->all(), [
            'address' => 'required',
        ])->validate();

        \App\Models\Address::firstOrCreate([
            'name' => $request['address'],
            'user_id' => \Auth::id()
        ]);
        $request->session()->flash('alert-success', 'آدرس شما با موفقیت ذخیره شد.');
        \Log::info('address was entered with name: '. $request['address'] . ' by user_id: ' . \Auth::id());

        return redirect()->back();
    }

    public function getAddress($id)
    {   
        \Meta::set('title','آدرس دهی');

        $order = \App\Models\Order::where('id', $id)->whereIn('status',['select','address','checkout','failed'])->first();
        if(!$order){
            \Log::warning('order not found with getAddress : ordder_id ' . $id );
            return redirect('/');
        }
        
        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);
        if(\Auth::id())
        {
            if(!$order->user_id)
            {
                $order->user_id = \Auth::id();
            }
            elseif( $order->user_id != \Auth::id() )
            {
                return redirect('/');
            }
            $price = ( $products->sum('price')* (100 - self::OFF) / 100 ) + self::PEYK;
            $order->price = $price;
            $order->status = "address";
            $order->save();
        }else{
            if($order->user_id){
                return redirect('/');
            }
        }

        return view('address')->withOrder($order)->withProducts($products);
    }

    public function getType($id)
    {   
        $id = explode(".", $id)[0];
        if(!is_numeric($id) ){
            return redirect('/');
        }
        $type = \App\Models\Type::where('id',$id)->first();
        if($type)
        {
            \Meta::set('title','دسته '.$type->name);
            $products = \App\Models\Product::where('type_id',$id)->where('ready',1)->get();

            return view('type')->withProducts($products);
        }
        else
        {
            return redirect('/');
        }
    }

    public function getCity($id) 
    {
        $id = explode(".", $id)[0];
        if(!is_numeric($id) ){
            return redirect('/');
        }
        $_GET['city']=$id;
        return view('home')->withId($id);
    }

    public function getProduct($id)
    {
        $product = \App\Models\Product::where('id',$id)->first();

        \Meta::set('title',$product->name);
        \Meta::set('keywords',$product->name . ',' . $product->description);
        \Meta::set('description',$product->description);

        \App\Models\ProductSeen::create(['product_id' => $product->id]);

        return view('aproduct')->withProduct($product);
    }

    private function _random_string()
    {
        $characters = '12345678';
        $randstring = '';
        for ($i = 0; $i < 3; $i++) {
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }

    public function postSetComment(Request $request)
    {
        \Validator::make($request->all(), [
            'comment' => 'required',
        ])->validate();
        \App\Models\Comment::create(['user_id' => \Auth::id(), 'comment' => $request['comment'] ]);
        $request->session()->flash('alert-success', 'ممنون از نظردهی شما. نظر شما را حتما با دقت مطالعه می کنیم.');

        return redirect('/admin/my-order');
    }

    public function postForgetPassword(Request $request)
    {
        \Validator::make($request->all(), [
            'phone' => 'required|exists:users,phone',
        ])->validate();

        $user = \App\User::where('phone',$request['phone'])->first();
        if($user)
        {
            $code = $this->_random_string();
            $user->password =  bcrypt($code);
            $user->save();
            $request->session()->flash('alert-success', 'رمز عبور برای ایمیل و شماره همراه شما ارسال شد.');
            $this->_sms_forget_password($request['phone'],$code);
        }

        return redirect()->back();
    }
    
   
   
    public function getVerificateId($id)
    {
        // $user->notify(new VerificationCode($user_id));
    }

    public function getLanguage($language)
    {
        session()->put('locale', $language);
        return redirect()->back();
    }

    private function _sms_forget_password($phone,$pass_code)
    {
        $user = \App\User::where('phone',$phone)->first();
        $user->notify(new ForgetPassword($pass_code));

        ini_set("soap.wsdl_cache_enabled", "0");
        $sms_client = new \SoapClient(
            'http://payamak-service.ir/SendService.svc?wsdl'
            , array('encoding'=>'UTF-8'));
        try 
        {
            $x = $sms_client->SendSMS([ 
                'userName' => self::SMS_USER,
                'password' => self::SMS_PASS,
                'fromNumber' => self::SMS_PHONE,
                'toNumbers' => [$phone],
                'messageContent' => self::NAME.': رمز عبور جدید شما ' . $pass_code . ' است.',
                'isFlash' => false,
                'recId' => [],
                // 'status' => []
                ]);
            \Log::info('sms با متن فراموشی رمز عبور فرستاده شد به '.$phone.' با رمز عبور: '.$pass_code. $x->SendSMSResult);
        } 
        catch (Exception $e) 
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    public function getNotifyOrder($id)
    {
        $order = \App\Models\Order::where('id', $id)->where('user_id',\Auth::id())
        ->where('address_id','!=','')->whereIn('status',['paid','paycredit','willpay'])->first();
        if($order){
            $this->_sms_kharidkarde(\Auth::user()->phone);
            return 'send';
        }else{
            \Log::warning('order not found with : ordder_id ' . $id );
            return 'not paid';
        }
    }

    private function _sms_kharidkarde($user_phone)
    {
        $user = \App\User::where('phone',$user_phone)->first();
        $user->notify(new KharidKonande());

        ini_set("soap.wsdl_cache_enabled", "0");
        $sms_client = new \SoapClient(
            'http://payamak-service.ir/SendService.svc?wsdl'
            , array('encoding'=>'UTF-8'));
        try 
        {
            $x = $sms_client->SendSMS([ 
                'userName' => self::SMS_USER,
                'password' => self::SMS_PASS,
                'fromNumber' => self::SMS_PHONE,
                'toNumbers' => [$user_phone],
                'messageContent' => 'سلام. درخواست شما در مرکز کارآفرینی ثبت شد.' . "\n" . ' همکاران ما به زودی با شمنا تماس خواهند گرفت.',
                'isFlash' => false,
                'recId' => [],
                // 'status' => []
                ]);
            \Log::info('sms با متن خریدار غذا فرستاده شد به '.$user_phone.' با کد: '. $x->SendSMSResult);
        } 
        catch (Exception $e) 
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    private function _getErrorZarrinpal($status)
    {
        switch ($status) {
        case '-1':
            return 'اطلاعات ارسال شده ناقص است.';
            break;
        case '-2':
            return 'آی پی یا مرچنت کد پذیرنده صحیح نیست';
            break;
        case '-3':
            return 'با توجه به محدودیت های شاپرک امکان پرداخت با رقم درخواست شده میسر نمی باشد.';
            break;
        case '-4':
            return 'سطح تایید پذیرنده پایین تر از صطح نقره ای است.';
            break;
        case '-11':
            return 'درخواست مورد نظر یافت نشد.';
            break;
        case '-12':
            return 'امکان ویرایش درخواست میسر نمی باشد.';
            break;
        case '-21':
            return 'هیچ نوع عملیات مالی برای این تراکنش یافت نشد.';
            break;
        case '-22':
            return 'تراکنش نا موفق می باشد.';
            break;
        case '-33':
            return 'رقم تراکنش با رقم پرداخت شده مطابقت ندارد.';
            break;
        case '-34':
            return 'سقف تقسیم تراکنش از لحاظ تعداد با رقم عبور نموده است.';
            break;
        case '-40':
            return 'اجازه دسترسی به متد مربوطه وجود ندارد.';
            break;
        case '-41':
            return 'اطلاعات ارسال شده مربوط به AdditionalData غیر معتر می باشد.';
            break;
        case '-42':
            return 'مدت زمان معتبر طول عمر شناسه پرداخت بین ۳۰ دقیقه تا ۴۰ روز می باشد.';
            break;
        case '-54':
            return 'درخواست مورد نظر آرشیو شده است.';
            break;
        case '100':
            return 'عملیات با موفقیت انجام گردیده است.';
            break;
        case '101':
            return 'عملیات پرداخت موفق بوده و قبلا Payment Verification تراکنش انجام شده است';
            break;
        default:
            return $id;
            break;
        };
    }
}

