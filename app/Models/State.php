<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','deleted_at','updated_at']; 
    use SoftDeletes;

    public function city()
    {
    	return $this->belongsTo('App\Models\City');
    }
}
