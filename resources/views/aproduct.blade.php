@extends('layout.master')
@section('fluid-container')
<div class="canvass" id="canvas2" onmousemove="magnify(event)">
    <img class="zoomedimage" id="canvas1">
</div>
<div class="row">
	<div class="text-center" style="background-color: white">
		<div class="seperate"></div>
		<div class="seperate"></div>
	    <img src="/storage/product/{{ $product->image->id }}-{{ $product->image->name }}" 
        id="image-id" onmousemove="magnify(event)" style="max-width: 100%;"> 
	</div>
</div>
<img src="/storage/product/{{ $product->image->id }}-{{ $product->image->name }}" id="imageid1"
 style="display: none;">

<div class="row product-details">
    <div class="col-xs-12 text-center">
        <h3 class="page-header">
        {{ $product->name }}
         <button class="btn btn-success" v-on:click="buy()">
            خرید
        </button>
        </h3>
        <h5 class="col-md-3 col-sm-6">
            دسته بندی: 
            @if($product->type->type)
                {{ $product->type->type->name }} ->
            @endif
            {{ $product->type->name }}
        </h5>            
	    <h5 class="col-md-3 col-sm-6">
	        بسته بندی: {{ $product->bastebandi }}
	    </h5>
	    <h5 class="col-md-3 col-sm-6">
	        تعداد در هر بسته: {{ $product->toharbastebandi }}
	    </h5>
        <h5 class="col-md-3 col-sm-6">
	        قیمت: {{ $product->price }} تومان
	    </h5>
        <div class="one-third-seperate"></div>
        <hr>
    </div>    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-blackboard"></span>
        موجودی: {{ $product->mojodi }}
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-blackboard"></span>
        کاربرد: {{ $product->applicant }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-resize-full"></span>
        سال ساخت: {{ $product->year }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-phone"></span>
        وزن : {{ $product->vazn }} 
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-usd"></span>
        کد محصول: {{ $product->code }} 
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-gbp"></span>
        ابعاد: {{ $product->size }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-object-align-horizontal"></span>
        گارانتی: {{ $product->garanti }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-sunglasses"></span>
        سازنده: {{ $product->sazande }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-tower"></span>
        بارکد: {{ $product->barcode }}
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-pushpin"></span>
        رنگ: {{ $product->rang }}
    </div>
     <div class="col-lg-3 col-md-4 col-sm-6">
        <span class="glyphicon glyphicon-pushpin"></span>
        جنس: {{ $product->jens }}
    </div>
    <div class="col-xs-11 col-xs-offset-1">
        {{ $product->description }}
    </div>    
    <div class="seperate"></div>
</div>
<div class="seperate"></div>
<div id="social-network-art-work">
    <button class="btn btn-info" v-on:click="comment()">
    <span class="glyphicon glyphicon-pencil"></span> نظردهی</button>
    <button class=" btn btn-danger" v-on:click="like()">
    <span class="glyphicon glyphicon-heart"></span> like
    </button> 
    <button class="btn btn-warning" v-on:click="share()">
        <span class="glyphicon glyphicon-share-alt"></span> 
        share
    </button>
</div>
<div class="seperate"></div>
<div class="row">
    <form enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
        <div>
        @if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul class="list-unstyled">
                        <li>{{ Session::get('alert-' . $msg) }}</li>
                    </ul>
                </div>
            @endif
        @endforeach
        </div>
        <div class="col-xs-12">
            <h4>
                نظر شما : 
            </h4>
        </div>
        <div class="col-xs-12">
            <textarea class="form-control" id="comment" name="comment"></textarea>
            <input type="hidden" name="product_id" value="{{$product->id}}">
        </div>
        <div class="half-seperate"></div>
        <div class="col-xs-12">
            <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-comment"></span>
            ارسال نظر</button>
        </div>
    </form>
</div>
<div class="seperate"></div>
@foreach([] as $comment)
<div class="row" style="margin-right: 60px">
    <div class="col-xs-12">
        <div class="media card-2" style="background-color: white">
            <div class="media-left">
                <img src="/public/img/2.jpg" class="media-object" style="width:40px">
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{ $comment->user->first_name }}
                {{ $comment->user->last_name }}</h4>
                <p>
                    <span class="glyphicon glyphicon-comment"></span>
                    {{$comment->comment}}
                </p>
            </div>
        </div>
    </div>
</div>
@endforeach
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12">
        <h3 class="text-right page-header">
            خریداران این محصول این ها را نیز خریداری کرده اند
        </h3>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    @foreach(\App\Models\Product::take(4)->get() as $another_product )
    <div class="col-lg-3 col-md-4 col-sm-6">
    <div class="block-out">
    <div class="block">
        <a href="/product/{{ $another_product->id }}-{{ $another_product->name }}" class="">
        <div class="text-center">
            @if($another_product->image) 
            <img src="/storage/product/{{ $another_product->image->id }}-{{ $another_product->image->name }}">
            @else
            <img src="/storage/art/default.png">
            @endif
            <h4 class="card-title">{{ $another_product->name }}</h4>
            <div class="col-xs-12 card-hr">
                <div class="col-xs-8 col-xs-offset-2">
                <hr>
                </div>
            </div>
            <div class="card-address">
                <span class="glyphicon glyphicon-calendar"></span>
                {{ $another_product->price }}
                تومان
            </div>
            <div class="half-seperate"></div>
            <div class="card-address">
                <span class="glyphicon glyphicon-blackboard"></span>
                {{ $another_product->type->name }}
                - 
                {{ $another_product->tecnique }}
            </div>
        </div>
        </a>
    </div>
    </div>
    </div>
    @endforeach
</div>
<div class="row">
    <div class="col-xs-12">
        <h3 class="text-right page-header">
            محصولات مشابه
        </h3>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    @foreach(\App\Models\Product::where('type_id' , $product->type_id)->where('id','!=',$product->id)->get() as $another_product )
    <div class="col-lg-3 col-md-4 col-sm-6">
    <div class="block-out">
    <div class="block">
        <a href="/product/{{ $another_product->id }}-{{ $another_product->name }}" class="">
        <div class="text-center">
            @if($another_product->image) 
            <img src="/storage/product/{{ $another_product->image->id }}-{{ $another_product->image->name }}">
            @else
            <img src="/storage/art/default.png">
            @endif
            <h4 class="card-title">{{ $another_product->name }}</h4>
            <div class="col-xs-12 card-hr">
                <div class="col-xs-8 col-xs-offset-2">
                <hr>
                </div>
            </div>
            <div class="card-address">
                <span class="glyphicon glyphicon-calendar"></span>
                {{ $another_product->price }}
                تومان
            </div>
            <div class="half-seperate"></div>
            <div class="card-address">
                <span class="glyphicon glyphicon-blackboard"></span>
                {{ $another_product->type->name }}
                - 
                {{ $another_product->tecnique }}
            </div>
        </div>
        </a>
    </div>
    </div>
    </div>
    @endforeach
</div>

<div class="row">
<div class="end-fluid"></div>
</div>
@endsection
@push('script')
<script type="text/javascript">
function magnify(event) {
    var Imageselected = document.getElementById("image-id");
    var imagewidthe = Imageselected.width;
    var imageheight = Imageselected.height;
    var offsetTop1 = Imageselected.offsetTop;
    var offsetLeft1 = Imageselected.offsetLeft;
    var x = event.clientX;
    var y = event.clientY;
    var lens = document.getElementById("canvas1");
    var hashie = document.getElementById("canvas2");
    var baseimage = document.getElementById("imageid1");
    var scrollWidth = window.scrollX;
    var scrollHeight = window.scrollY;
    y = y + scrollHeight ; 
    x = x + scrollWidth ;
    posY = y - offsetTop1 ;
    posX = x - offsetLeft1 ;
    hashie.style.display = "none"; 
    if (x>Imageselected.offsetLeft && x<Imageselected.offsetLeft+imagewidthe && y>(offsetTop1+20) && y<(offsetTop1+imageheight-20)) {
        hashie.style.display = "block";
    }
    else {
        hashie.style.display = "none";
    }
    lens.src = "/storage/product/{{ $product->image->id }}-{{ $product->image->name }}";
    lens.style.top = -2*posY+75+"px";
    lens.style.left = -2*posX+75+"px";
    lens.style.height = (2*(baseimage.height))+"px";
    lens.style.width = (2*(baseimage.width))+"px";;

    hashie.style.top = y-75+"px";
    hashie.style.left = x-75+"px";          
}
</script>
<style type="text/css">
    .canvass {
        position:absolute;
        width: 170px;
        height: 170px;
        overflow:hidden;
        border : 1px solid #eee;
        border-radius: 50%;
        display: none;
    }
    .zoomedimage {
        position: absolute;
    }
</style>

<script>
// new Vue({
//     el: '#social-network-art-work',
//     data: {
//         like_count: 0,
//     },
//     methods: {
//         fetchData: function () {
//             this.$http.get('/api/art-work/like/{{$product->id}}').then(function (response) {
//                 // this.like_count = response.data;
//             });
//         },
//         like:function () {
//             if( {{ \Auth::id() ? \Auth::id() : 0 }} ){
//                 this.$http.post('/api/art-work/like/{{$product->id}}',
//                     {user_id: {{ \Auth::id() ? \Auth::id() : 0 }} }).then(function (response) {
//                         this.fetchData();
//                 });
//             }else{
//                 alert('باید لاگین کنید.');
//             }
//         },
//         share:function () {
//             var url =  '{{ Request::url() }}';
//             // document.getElementById('')
//             document.execCommand('copy');
//         },
//         comment:function () {
//             document.getElementById('comment').focus();
//         },
//     },
//     mounted: function () {
//         // this.fetchData();
//     }
// });
</script>
@endpush