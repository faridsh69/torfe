<?php

use Illuminate\Database\Seeder;

class FoodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $foods =[
        	['name' => 'پیتزا مخصوص', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'19000'],
        	['name' => 'پیتزا پپرونی', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'20000'],
        	['name' => 'پیتزا مخلوط', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'19000'],
        	['name' => 'پیتزا سبزیجات', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'18000'],
        	['name' => 'پیتزا گوشت وقارچ', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>'23000'],
        	['name' => 'پیتزا مرغ', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'19000'],
        	['name' => 'پیتزا چهارفصل', 'content' => 'پنیر پیتزا - کالباس - فلفل دلمه ای', 'type_id' => '1', 'price' =>	'17000'],
        	['name' => 'بندری', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'9000'],
        	['name' => 'کالباس', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'10000'],
        	['name' => 'هات داگ', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'11000'],
        	['name' => 'کوکتل', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'12000'],
        	['name' => 'همبرگر', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'11000'],
        	['name' => 'چیزبرگر', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'10000'],
        	['name' => 'تاکو', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'9000'],
        	['name' => 'دوبل برگر', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'9000'],
        	['name' => 'هات داگ مخصوص', 'content' => 'گوجه - خیارشور - کاهو - سوسیس', 'type_id' => '2', 'price' =>	'9000'],
        	['name' => 'مرغ سوخاری ۳تکه', 'content' => 'مرغ - سیب زمینی - سالاد', 'type_id' => '3', 'price' =>	'9000'],
        	['name' => 'مرغ سوخاری ۲تکه', 'content' => 'مرغ - سیب زمینی - سالاد', 'type_id' => '3', 'price' =>	'9000'],
        	['name' => 'قارچ سوخاری', 'content' => 'مرغ - سیب زمینی - سالاد', 'type_id' => '3', 'price' =>	'9000'],
        	['name' => 'سوسیس سوخاری', 'content' => 'مرغ - سیب زمینی - سالاد', 'type_id' => '3', 'price' =>	'9000'],
        	['name' => 'چلوکباب کوبیده', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'چلوکباب برگ', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'], 
        	['name' => 'چلوکباب سلطانی', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'چلو جوجه کباب', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'], 
        	['name' => 'جوجه کباب بی استخوان', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'جوجه کباب بی بااستخوان', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'جوجه کباب زعفرانی', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'مییکس', 'content' => 'برنج - کباب - گوجه - پیاز', 'type_id' => '10', 'price' =>	'9000'],
        	['name' => 'خورشت قیمه', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'خورشت قرمه سبزی', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'خورشت بادمجون', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'خورشت کرفس', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'خورشت بامیه', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'خورشت فسنجان', 'content' => 'برنج - خورشت ', 'type_id' => '11', 'price' =>	'9000'],
        	['name' => 'ماهی قزل آلا', 'content' => 'برنج - ماهی ', 'type_id' => '12', 'price' =>	'9000'],
        	['name' => 'ماهی تن', 'content' => 'برنج - ماهی ', 'type_id' => '12', 'price' =>	'9000'],
        	['name' => 'ماهی دریای جنوب', 'content' => 'برنج - ماهی ', 'type_id' => '12', 'price' =>	'9000'],
        	['name' => 'ماهی سرسخ', 'content' => 'برنج - ماهی ', 'type_id' => '12', 'price' =>	'9000'],
        	['name' => 'خاویار', 'content' => 'برنج - ماهی ', 'type_id' => '12', 'price' =>	'9000'],
        	['name' => 'دیزی', 'content' => 'برنج - ماهی ', 'type_id' => '13', 'price' =>	'9000'],
        	['name' => 'بریونی', 'content' => 'برنج - ماهی ', 'type_id' => '13', 'price' =>	'9000'],
        	['name' => 'باقالی قاتوم', 'content' => 'برنج - ماهی ', 'type_id' => '17', 'price' =>	'9000'],

            ['name' => 'سالاد کلم', 'content' => 'کلم کالباس سس', 'type_id' => '30', 'price' =>   '2000'],
            ['name' => 'سالاد کاهو', 'content' => 'کاهو هویج سس', 'type_id' => '30', 'price' =>   '3000'],
            ['name' => 'ماست', 'content' => 'موسیر', 'type_id' => '32', 'price' =>   '1000'],
            ['name' => 'ماست و خیار', 'content' => '', 'type_id' => '32', 'price' =>   '2000'],
            ['name' => 'ماست میوه ای', 'content' => '', 'type_id' => '32', 'price' =>   '3000'],
            ['name' => 'زیتون پرورده', 'content' => '', 'type_id' => '32', 'price' =>   '3000'],
            ['name' => 'دوغ', 'content' => '', 'type_id' => '33', 'price' =>   '1500'],
            ['name' => 'نوشابه', 'content' => '', 'type_id' => '33', 'price' =>   '1500'],
            ['name' => 'دلستر', 'content' => '', 'type_id' => '33', 'price' =>   '1500'],

    	];
    	$restaurants = \App\Models\Restaurant::get();
        
        $image = [
            'name' => 'logo.png',
            'description' => 'food_image', 
            'mime_type' => 'png', 
            'size' => '1'];
        $image = \App\Models\Image::firstOrCreate($image);

        foreach($restaurants as $restaurant)
        {
        	var_dump($restaurant->id);
    	   	foreach ($restaurant->types as $type) {
    	   		var_dump($type->id);
    	   		foreach($foods as $food)
    	   		{
    	   			if ( $food['type_id'] == $type->id)
    	   			{
    	   				$food['restaurant_id'] = $restaurant->id;
                        // $food['image_id'] = 1;
    	   				\App\Models\Food::updateOrCreate($food);
    	   			}
    	   		}
    	   	}
        }
    }
}
