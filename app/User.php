<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // use SoftDeletes;
    
    protected $guarded = [];
    // protected $fillable = [ 'first_name','last_name','username','password','email','cellphone'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function restaurant()
    {
        return $this->hasOne('App\Models\Restaurant');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }
}
