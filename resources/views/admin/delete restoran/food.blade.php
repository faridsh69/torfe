<!-- @extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="post" id="form">
		{{ csrf_field() }}
		<div class="panel {{ Request::segment(4) == 'edit' ? 'panel-info' : 'panel-default' }} panel-default">
		<div class="panel-heading">
			@if(Request::segment(4) == 'edit')
			ویرایش غذاهای: 
			{{ $restaurant->name }}
			<a class="btn btn-success btn-sm" href="/admin/restaurant/my-food/{{ $restaurant->id }}">افزودن غذای جدید</a>
			@else
			افزودن غذا به غذاهای: 
			{{ $restaurant->name }}
			@endif
			<input type="hidden" name="restaurant_id" value="{{ $restaurant->id }}">
		</div>
		<div class="panel-body">
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">نام غذا</td>
					<td><input type="text" name="name" class="form-control"
					value="{{  $food->name  or old('name')  }}" >
					<div class="help-block">
						در هنگام ویرایش نام غذا را اگر عوض کنید غذای جدیدی ساخته می شود.
					</div>
					</td>
				</tr>
				<tr>
					<td>محتویات یا توضیحات</td>
					<td><input type="text" name="content" class="form-control"
					value="{{ $food->content or old('content') }}">
					</td>
				</tr>
				<tr>
					<td>نوع غذا</td>
					<td>
					<select class="form-control" name="type" >
						@foreach($restaurant->types as $type)
						<option value="{{ $type->id }}"
						{{ ( isset($food) ? $food->type_id : 1 ) == $type->id ? "selected" : ""}} > 
							{{ $type->name }}
						</option>
						@endforeach
					</select>
					</td>
				</tr>
				<tr>
					<td>مبلغ</td>
					<td><input type="number" name="price" class="form-control"
					value="{{ $food->price or old('price') }}" ></td>
				</tr>
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر غذا حداکثر ۳۰۰ کلوبایت باشد.</div>
					</td>
					<td >
						<input id="file" type="file" accept='image/*' />
						<button id="cropbutton" type="button" class="btn">برش عکس</button>
						<button id="rotatebutton" type="button" class="btn">چرخش</button>
						<button id="hflipbutton" type="button" class="btn">آنه افقی</button>
						<button id="vflipbutton" type="button" class="btn">آینه عمودی</button>
						<br>
						<div id="views"></div>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($food))
						عکس سابق
						@if($food->image_id)
						<img src="/storage/food/{{$food->image_id}}-{{$food->image->name}}"  class="img-responsive img-thumbnail">
						@else
						 ندارد
						@endif
						@endif
						<p id='no-preview'>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>
				</tr>
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block">ذخیره اطلاعات غذا</button>
        </div>
		</div>

		</form>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="reponsive-table">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
				شماره درسیستم
				</th>
				<th>
				نام غذا
				</th>
				<th>
				تصویر
				</th>
				<th>
				محتویات
				</th>
				<th>
				مبلغ
				</th>
				<th>
				نوع
				</th>
				<th>
				وضعیت 
				</th>
				<th>
				ویرایش	
				</th>
				<th>
				غیر فعال	
				</th>
			</tr>
			</thead>
			<tbody>
			@foreach($foods as $food)
			<tr>
				<td>
				{{ \Nopaad\Persian::correct( $food->id) }}
				</td>
				<td>
				{{ \Nopaad\Persian::correct( $food->name) }}
				</td>
				<td>
				@if($food->image_id)
				<img src="/storage/food/{{$food->image_id}}-{{$food->image->name}}" style="width: 60px">
				@else
				عکس ندارد
				@endif
				</td>
				<td>
				{{ \Nopaad\Persian::correct( $food->content) }}
				</td>
				<td>
				{{ \Nopaad\Persian::correct( $food->price) }}
				</td>
				<td>
				{{ $food->type->name }}
				</td>
				<td>
				{{ $food->ready == 1 ? 'حاضر' : 'تمام شده است' }}
				</td>
				<td>
					<a class="btn btn-info btn-sm" href="/admin/restaurant/my-food/edit/{{ $food->id }}">
					ویرایش
					</a>
				</td>
				<td>
					@if($food->ready == 1)
					<a class="btn btn-danger btn-sm" href="/admin/restaurant/my-food/remove/{{ $food->id }}">
					غیرفعال
					</a>
					@else
					<a class="btn btn-success btn-sm" href="/admin/restaurant/my-food/active/{{ $food->id }}">
					فعال سازی
					</a>
					@endif
				</td>
			</tr>
			@endforeach
			</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
@push('script')

<link rel="stylesheet" type="text/css" href="/public/css/jcrop.css">
<script src="/public/js/jcrop.js"></script>
<script src="/public/js/jcrop.main.js"></script>

<script>
 	// $('#preview').hide();
  // 	var uploadImage = function(event) {
	 //    var input = event.target;
	 //    var reader = new FileReader();
	 //    reader.onload = function(){
		//     $('#preview')[0].src = reader.result;
  //   	};
	 //    reader.readAsDataURL(input.files[0]);
	 //    $('#no-preview').hide();
	 //    $('#preview').show();
  // 	};
</script>
@endpush -->