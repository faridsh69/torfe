<nav class="navbar background-nav " 
{{ Request::segment(1) != 'admin' ? '' : 'data-spy=affix data-offset-top="0"' }}>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>                           
            </button>
            <a class="navbar-brand {{Request::segment(1) == '' ? 'selected':''}}" href="/" style="
                margin-top: 0px;"> 
               <!--  <img src="/public/img/logo.png" alt="logo" class="logo-icon" style="width: 60px;display: inline-block;"> -->
                <span>
                خانه
                </span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
               <!--  <li class="drop hidden-xs">
                    <a href="javascript:void(0)" class="drop-a">افراد 
                        <span class="glyphicon glyphicon-chevron-down small-size"></span>
                        <img src="/public/img/x13.png" class="drop-img">
                    </a>
                </li> -->
                
                <li><a href="/about-us" class="draw2">
                درباره ما </a>
                </li>
                <li><a href="/mentor" class="draw2">
                مربیان</a>
                </li>
                <li><a href="/blog" class="draw2">
                بلاگ</a>
                </li>
                <li><a href="/news" class="draw2">
                اخبار</a>
                </li>
                <li><a href="javascript:void(0)" data-toggle="modal" data-target="#contact-us-modal">تماس با ما</a></li>
               
                <!-- <li><a class="btn btn-default" style="color: black;padding: 15px;margin: 10px" href="/register-idea">ثبت درخواست</a></li> -->
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <div class="one-third-seperate"></div>
            @if(empty(Auth::user()))
                <div class="half-seperate"></div>
                <a href="/user/register" class="google-a"><span class="glyphicon glyphicon-user"></span> ثبت نام</a>
                |
                <a href="/user/login" class="google-a"><span class="glyphicon glyphicon-log-in"></span> ورود</a>
            @else
                <a href="/admin/profile">
                <span class="glyphicon glyphicon-cog big-size"></span>
                {{ Auth::user() ? Auth::user()->first_name : ''}} 
                {{ Auth::user() ? Auth::user()->last_name : ''}}
                <label class="label label-default" style="margin:3px;padding: 0px 7px 0px 7px">
                اعتبار: {{ \Nopaad\Persian::correct(number_format( Auth::user()->credit , 0, '',',')) }} تومان
                </label>
                @if(\Auth::user()->roles()->count() > 0)
                <br>
                    @foreach(\Auth::user()->roles()->pluck('name') as $role)
                        <small style="position: relative;">
                            <span class="label {{ in_array($role ,['manager']) ? 'label-danger' : 'label-success' }}">{{ trans('roles.' . $role) }}</span>
                        </small>
                    @endforeach
                @endif
                </a>
            @endif
            </ul>
        </div>
    </div>
</nav>
@if(Request::segment(1) != 'admin')
<img src="/public/img/shadow-3.png" width="100%" class="background-nav-shadow">
@else
<div class="affix-fixer"></div>
@endif


<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">تماس با مرکز نوآوری و کارآفرینی طرفه نگار</h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center bold">
                    مرکز نوآوری و کارآفرینی طرفه نگار
                </h3>
                <h5 style="line-height: 150%">
                    تلفن دفتر: ۲۲۹۲۱۲۴۳
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    ساعت کاری: هر روز هفته به جز جمعه ها از ساعت ۹ الی ۱۸

                </h5>
                <div class="half-seperate"></div>
                <h5>
                   آدرس مرکز :<br><br>
                   خیابان میرداماد - خیابان نسا - کوچه زرنگار - پلاک ۳۵ - ساختمان طرفه نگار - طبقه سوم

                </h5>
                <div class="half-seperate"></div>
            </div>
        </div>
    </div>
</div>
