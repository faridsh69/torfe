@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="post" action="/admin/news" id="form">
		{{ csrf_field() }}
		<div class="panel {{ Request::segment(4) == 'edit' ? 'panel-info' :'panel-default'}} panel-default">
		<div class="panel-heading">
			@if(Request::segment(3) == 'edit')
			ویرایش خبر: 
			{{ $news->name }}
			<a class="btn btn-success btn-sm" href="/admin/news">افزودن خبر جدید</a>
			@else
			افزودن اخبار
			@endif
			@if( isset($news) )
				<input type="hidden" name="id" value="{{ $news->id }}">
			@endif
		</div>
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">عنوان خبر</td>
					<td><input type="text" name="title" class="form-control"
					value="{{  $news->title  or old('title')  }}" required></td>
				</tr>
				<tr>
					<td width="150">بالای عنوان</td>
					<td><input type="text" name="top_title" class="form-control"
					value="{{  $news->top_title  or old('top_title')  }}" required></td>
				</tr>
				<tr>
					<td width="150">زیر عنوان</td>
					<td><input type="text" name="sub_title" class="form-control"
					value="{{  $news->sub_title  or old('sub_title')  }}" required></td>
				</tr>
				<tr>
					<td>متن خبر</td>
					<td><textarea type="text" name="body" required="true" class="form-control"
					>{{ $news->body or old('body') }}</textarea></td>
				</tr>
				<tr>
					<td>آماده برای نمایش در سایت</td>
					<td>
					<div class="radio-style">
					<div class="radio-button">
				    	<input type="radio" id="yes" name="ready" value="1" checked="{{ $news->ready or old('ready') == 1 }}">
				    	<label for="yes"><span class="radio">بله</span> </label>
				    </div>
				    <div class="radio-button">
				    	<input type="radio" id="no" name="ready" value="0">
				    	<label for="no"><span class="radio">خیر</span> </label>
				    </div>
					</div>
					</td>
				</tr>
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر اثر حداکثر ۳۰۰ کلوبایت باشد.</div>
					</td>
					<td>
						<input id="file" type="file" accept='image/*' />
						<div class="half-seperate"></div>
						<button id="cropbutton" type="button" class="btn btn-info btn-xs">برش عکس</button>
						<button id="rotatebutton" type="button" class="btn btn-info btn-xs">چرخش</button>
						<button id="hflipbutton" type="button" class="btn btn-info btn-xs">آینه افقی</button>
						<button id="vflipbutton" type="button" class="btn btn-info btn-xs">آینه عمودی</button>
						<div class="half-seperate"></div>
						<div id="views"></div>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($news))
						عکس سابق
						@if($news->image_id)
						<img src="/storage/news/{{$news->image_id}}-{{$news->image->name}}"  class="img-responsive img-thumbnail">
						@else
						ندارد
						@endif
						@endif
						<p>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>		
				</tr>
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block">ذخیره اطلاعات</button>
		</div>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">
			محصولات
		</div>
		<div class="reponsive-table">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
				توضیحات 
				</th>
				<th style="width: 30%;">
				تصویر
				</th>
				<th width="100px">
				ویرایش و حذف
				</th>
			</tr>
			</thead>
			<tbody>
			@foreach($newses as $news)
			<tr>
				<td>
			        <div class="row">
					    <div class="col-xs-12">
					        <h4 class="page-header">
					            <small>{{ $news->top_title }}</small>	
					            <br>			           
					            {{ $news->title }}
					            <br>
					            <br>
					            <small>{{ $news->sub_title }}</small>				           
					        </h4>
				            <p>
						        متن خبر: <br> {{ $news->body }}
						    </p>
					    </div>
					    
					</div>
			    </div>
				</td>
				<td>
					@if($news->image)
					<img src="/storage/news/{{ $news->image->id }}-{{ $news->image->name}}" 
					style="max-width: 100%;max-height: 280px;">
					@endif
				</td>
				<td>
					<a class="btn btn-info btn-sm" href="/admin/news/edit/{{ $news->id }}">
					ویرایش
					</a>
					<div class="half-seperate"></div>
					@if($news->ready == 1)
					<a class="btn btn-danger btn-sm" href="/admin/news/remove/{{ $news->id }}">
						غیرفعال
					</a>
					@else
					<a class="btn btn-success btn-sm" href="/admin/news/active/{{$news->id}}">
						فعال سازی
					</a>
					@endif					
				</td>
			</tr>
			@endforeach
			</tbody>
			</table>
		</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="text-center">
			{{ $newses->links() }}
		</div>
	</div>
</div>
@endsection
@push('script')
<link rel="stylesheet" type="text/css" href="/public/css/jcrop.css">

<script src="/public/js/jcrop.js"></script>
<script src="/public/js/jcrop.main.js"></script>
@endpush