<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSeen extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','deleted_at','updated_at']; 

    use SoftDeletes;

    public function product()
    {
    	return $this->belongsTo('App\Models\Product');
    }
}
