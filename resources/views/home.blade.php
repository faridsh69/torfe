@extends('layout.master')
@section('fluid-container')
<!-- <div class="row">
    <div class="col-xs-12">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
    @if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    </div>
</div> -->

<!-- <div style="position: absolute;z-index: 1;max-width: 80%">
    <div id="large-header">
        <canvas id="demo-canvas" style="max-width: 100%;"></canvas>
    </div>
</div> -->
<!-- background: linear-gradient(141deg, #0fb8ad 0%, #1fc8db 51%, #2cb5e8 75%); -->
<div class="row text-center" style="background-image: url('/public/img/a33.jpg');
background-position: cover;background-attachment: fixed;">
    <div class="col-xs-10 col-xs-offset-1 ">            

        <h1 class="bold  hug-size" style="color: white" >
            مرکز نوآوری و کارآفرینی طرفه نگار
        </h1>
        <h3 style="color: white">  
            <!-- ما آماده ایم تا شما را هرچه سریع‌تر به اهدافتان برسانیم -->
            فراسوی نیازهای شما
        </h3>
        </div>
    <div class="col-xs-offset-0 col-xs-11">
        <div class="">
            @if(!$mobile)
            <div class="ip-slideshow-wrapper">
                <nav>
                    <span class="ip-nav-left"></span>
                    <span class="ip-nav-right"></span>
                </nav>
                <div class="ip-slideshow"></div>
            </div>
            @endif
        </div><!-- /container -->        
    </div>
</div>
<div class="row" style="background-color: white">
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <h2 class="text-center page-header">
        معرفی مرکز نوآوری و کارآفرینی طرفه نگار
        </h2>
        <div class="seperate"></div>
        <h4 style="line-height: 30px;text-align: center;">
شرکت طرفه‌ نگار از سال 1377 با هدف نهادینه کردن استفاده از تکنولوژی اطلاعات در روندهای شغلی، پا به عرصه طراحی و تولید نرم افزار نهاد. در سال 1394، مرکز نوآوری و کارآفرینی طرفه ‌نگار نیز در همین راستا و به منظور ایجاد بستر رشد و شکوفایی برای کمک به کسب و کارهای نوپایی که برنامه تولید محصول و یا ارائه خدمات را دارند؛ آغاز به فعالیت نمود و به عرصه‌ی نوآوری و کارآفرینی کشور ورود پیدا کرد. در طی این سال ها نیز با موفقیت و پیشرفت روزافزون به فعالیت خود ادامه داده و ماموریت ها و اهداف خود را تحقق بخشیده است.
        </h4>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12 text-center">
        <img src="/public/img/20.jpg" style="max-width: 100%" class="img-circle">
    </div>
</div>
<div class="seperate"></div>
<div class="row text-center" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-xs-12 ">
        <h2 class="bold">
            <!-- <span class="glyphicon glyphicon-copy"></span> -->
            همکاری با مرکز نوآوری و کارآفرینی طرفه نگار
        </h2>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-sm-4">
        <img src="/public/img/21.jpg" width="100px" alt="مرحله ۱" class="img-circle">
        <div class="half-seperate"></div>
        <h4 class="bold">
            همکاری
        <div class="seperate"></div>
        <small>رشد و توسعه کسب وکارتان و هوشمندسازی سرمایه‌تان را به ما بسپارید.</small>
        </h4>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/22.jpg" width="100px" alt="مرحله ۲" class="img-circle">
        <div class="half-seperate"></div>
        <h4 class="bold">
            شتاب‌دهی
        <div class="half-seperate"></div>
        <small>با ما دوران شتاب‌دهی‌تان را با شتاب و انگیزه ی بیشتری طی نمایید.</small>
        </h4>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/19.jpg" width="100px" alt="مرحله ۳" class="img-circle">
        <div class="half-seperate"></div>
        <h4 class="bold">
            سرمایه گذاری
            <div class="half-seperate"></div>
        <small>دیگر نگران جذب سرمایه برای کسب‌و‌کارتان نخواهید بود.</small>
        </h4>
    </div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        @include('common.slider')
    </div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a href="register-idea" class="button button--moema">
            <span class="glyphicon glyphicon-pencil "></span>
            ثبت درخواست
        </a>
    </div>
</div>
<div class="seperate"></div>

<div class="row" style="background: url('img/wall.jpg');padding: 20px;" >
    <div class="seperate" id="introduce"></div>
    <div class="seperate" ></div>
    <div class="seperate" ></div>
    <div class="col-sm-4" style="text-align: justify;">
        <h3 class="text-center bold">
            روند کاری
        </h3>
        <h5 style="line-height: 180%">
            همه چیز از یک ایده عالی شروع می‌شود.
            اگر به ایده خود ایمان دارید آن را در این سامانه مطرح کنید. 
            پس از بررسی طرح شما جلساتی مهیا می‌شود که در آنها هرگونه ابهامی برای شما رفع شود.

            <br>
            پس از آن محلی برای تولید و اجرای ایده شما در اختیارتان قرار می‌گیرد که در آن به ساخت محصول می‌پردازید.
            در حین کار به شما راهنمایی هایی ارایه می‌گردد که به بهبود محصول شما کمک می‌کند.
            با کامل شدن محصولتان ارایه ای از آن محصول را برای اعضای هیت مدیره انجام میدهید.
        </h5>
        <h4>
            ساعت کاری مرکز:
            همه روزه از ساعت ۶ صبح الی ۱۰ شب
            <br>
            <br>
            روزهای کاری :
            همه روزه
            <br>
            <br>
            <small>
            آدرس محل کار:
            خیابان میرداماد - کوچه زرنگار - پلاک ۳۵ طبقه سوم - مرکز کارآفرینی طرفه نگار
            </small>
             
        </h5>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-8">
        <div class="seperate"></div>
        <img src="/public/img/ravand.jpg" class="img-responsive">

        <!-- <img alt="سنگ ساختمانی" src="/public/img/n12.jpg" class=" img-responsive"> -->
    </div>
</div>


<div class="row">
    <div class="col-xs-12 text-center">
        <h4 class="page-header hug-size">
            تازه ها
        </h4>
    </div>
</div>
<!-- <div class="seperate"></div> -->
<div class="row" style="background: white;">
    @foreach(\App\Models\News::get() as $news)
        <div class="col-xs-6">
            <div class="cart">
                <div class="media">
                    <div class="media-left">
                        @if($news->image)
                            <img src="/storage/news/{{ $news->image->id }}-{{ $news->image->name}}" 
                            style="max-height: 280px;">
                        @endif
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <small>{{ $news->top_title }}</small>   
                            <div class="half-seperate"></div>
                            {{ $news->title }}
                            <div class="one-third-seperate"></div>
                            <small>{{ $news->sub_title }}</small> 
                        </h4>
                        <div class="seperate"></div>
                        <p style="text-align: justify;">
                            {{ $news->body }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>

<div class="row" style="background-color: white;padding: 20px;">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                  <h3>
                    درکنار هم می‌توانیم 
                    <img width="90" alt="why" src="/public/img/why.png" class=""> 
                </h3>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-md-offset-0 text-center">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img alt="مرحله ۱" width="57" src="/public/img/s1.png" class="">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                مشاوره برای رسیدن به اهداف
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img alt="مرحله ۲" width="60" src="/public/img/s2.png" class="">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                محیط کار صمیمی و فعال
                </p>
                <small>
                    محل کار مهم‌ترین بخش انتخاب ک شغل است مخصوصا زمانی که نیاز به محلی آرام و پویا دارید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>

            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s3.png" alt="مرحله ۳" width="57">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                ارتباطات موثر
                </p>
                <small>در این مرکز می‌‌توانید با افرادی که استارتاپ موفق داشته اند روبرو شوید و از آنها الگوبرداری کنید</small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s4.png" alt="مرحله ۱">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                سرمایه گذاری هدفمند
                </p>
                <small>
                    در این محیط برخلاف سایر مراکز کارآفرینی رسیدن شما به محصول نهاییتان هدف قطعی ماست
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s5.png" alt="مرحله ۲">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                مربیان برجسته در کنار شما
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s6.png" alt="مرحله ۳">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                آموزش رشد استارتاپ
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
            </div>
        </div>
        <div class="seperate"></div>
        <div class="seperate"></div>
    </div>
</div>



<div class="row" style="background: white;padding: 20px; display: none;" >
    <div class="seperate" id="news"></div>
    <div class="seperate" ></div>
    <div class="seperate" ></div>
    <div class="col-sm-12">
        <h3 class="text-center bold">
            استارتاپ‌ها
            <br>
            جای شما خالی است - شما هم می توانید کی از ما باشید 
            <br>
            <br>
            <a href="/register-idea" class="btn btn-primary btn-lg">ثبت استارتاپ</a>
        </h3>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="stage">
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                تیمچه سرا
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">داوودی - شهیدی</span>                             
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.
                                    این سامانه به دنباله خدمت رسانی طرفه نگار به مشتریان هلو ارایه می‌گردد.
                                </p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آرایشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آموزش زبان 
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                ورزشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="seperate"></div>
</div>

@endsection
@push('script')
 <script type="text/javascript" src="/public/js/jquery.imgslider.js"></script>
<script type="text/javascript">
    $(function() {

        $( '#fs-slider' ).imgslider();

    });
</script>
<script src="/public/js/classie2.js"></script>
<!-- <script src="/public/js/stepsForm.js"></script> -->
<!-- <script>
    var theForm = document.getElementById( 'theForm' );

    new stepsForm( theForm, {
        onSubmit : function( form ) {
            // hide form
            classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );

            /*
            form.submit()
            or
            AJAX request (maybe show loading indicator while we don't have an answer..)
            */

            // let's just simulate something...
            var messageEl = theForm.querySelector( '.final-message' );
            messageEl.innerHTML = 'ممنون از پر کردن فرم ما ،به زودی می‌بینیمت';
            classie.addClass( messageEl, 'show' );
        }
    } );
</script> -->
<script src="/public/js/particlesSlideshow.js"></script>

<script>
    scrollDown = function() {
        $('html,body').animate({
            scrollTop: $("#restaurant-list").offset().top
        },'slow');
    }
    move = $(window).scrollTop() / 12;
    $('#scroled2').animate({
        left: 10+move+'px',
        opacity: '1',
    }, 800);
    $('#scroled').animate({
        right: 10+move+'px',
        opacity: '1',
    }, 800);
    $(window).scroll(function () {
        if($(window).scrollTop() < 1000){
            move = $(window).scrollTop() / 4;
            $('#scroled2').css("left",10+move/2+"px");
            $('#scroled').css("right",10+move/2+"px");
        }
    });
</script>
<!-- <script src="/public/js/demo-2.js"></script> -->
@endpush

