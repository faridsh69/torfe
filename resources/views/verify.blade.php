@extends('layout.master')
@section('container')

<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12 text-center">
		<h2>
			@if($code == 4)
			پرداخت در محل
			@else
			بازگشت از پرداخت
			@endif
		</h2>
	</div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12">
		@if($code == 3 || $code == 2)
		<div class="alert alert-danger">
			<div class="seperate"></div>
			<h3 class="text-center">
				پرداخت شما با موفقیت انجام نشد.
			</h3>
			<div class="seperate"></div>
			<ul>
				<li>
					شما می توانید با مراجعه به صفحه <a href="/checkout/{{$order->id}}">مجددا پرداخت خود را انجام دهید</a>.
				</li>
				<li>
					سفارش شما در سیستم ثبت مانده است.
				</li>
				<li>
					درصورت هرگونه مشکلی می توانید با شماره تلفن {{ \App\Http\Controllers\Controller::PHONE}} تماس حاصل نمایید.
				</li>
			</ul>
		</div>
		@elseif($code == 1)
		<div class="alert alert-success">
			<div class="seperate"></div>
			<h3 class="text-center">
				پرداخت شما با موفقیت انجام شد.
			</h3>
			<div class="seperate"></div>
			<ul>
				<li>
					سفارش شما برای انبار ارسال شده است.
				</li>
				<li>
					از منوی کاربری می توانید جزییات سفارش خود را مشاهده نمایید.
				</li>
				<li>
					انبار در حال آماده سازی سفارش شماست.
				</li>
				<li>
					در صورت بروز هر گونه مشکلی می توانید سفارش خود به شماره {{ $order->id }} را با تماس با تیم پشتیبانی ما پیگیری نمایید.
				</li>
			</ul>
			<div class="seperate"></div>
		</div>
		@elseif($code == 4)
		<div class="alert alert-success">
			<div class="seperate"></div>
			<div class="seperate"></div>
			<ul>
				<li>
					سفارش شما برای انبار ارسال شده است.
				</li>
				<li>
					زمان ارسال نهایتا دو روز کاری است.
				</li>
				<li>
					از منوی کاربری می توانید جزییات سفارش خود را مشاهده نمایید.
				</li>
				<li>
					انبار در حال آماده سازی سفارش شماست.
				</li>
				<li>
					در صورت بروز هر گونه مشکلی می توانید سفارش خود را با تماس با تیم پشتیبانی ما پیگیری نمایید.
				</li>
				<li>
					شماره سفارش شما: "{{ $order->id }}" می باشد.
				</li>
			</ul>
			<div class="seperate"></div>
		</div>
		@endif
	</div>
</div>
<div class="seperate"></div>
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
                <li>{{ Session::get('alert-' . $msg) }}</li>
            </ul>
        </div>
    @endif
@endforeach
@if(\App\Models\Comment::where('user_id',\Auth::id())->get()->count() == 0)
<form enctype="multipart/form-data" method="POST" action="/set-comment">
    {{ csrf_field() }}
    <div>
    @if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    </div>
    <div class="col-xs-12">
        <h4>
            نظر شما راجع به سایت: <small>( دانستن نظر شما برای خدمت رسانی بهتر ما نیاز است. )</small>
        </h4>
    </div>
    <div class="col-xs-12">
        <textarea class="form-control" id="comment" name="comment"></textarea>
    </div>
    <div class="half-seperate"></div>
    <div class="col-xs-12">
        <button class="btn btn-default" type="submit">
        <span class="glyphicon glyphicon-comment"></span>
        ارسال نظر</button>
    </div>
</form>
@endif
<div class="seperate"></div>
<div class="seperate"></div>
@endsection
@push('script')
@if($code == 1 || $code == 4)
<script type="text/javascript">
    $.get("/admin/notify/{{ $order->id }}", function(data, status){});
</script>
@endif
@endpush