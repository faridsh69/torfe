<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','deleted_at','updated_at']; 
    use SoftDeletes;
}
