<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getLogin()
    {
        \Meta::set('title','ورود کاربر');

        return view('user.login');
    }

    public function postLogin(Request $request)
    {
        if (\Auth::attempt(['phone' => $request['phone'], 'password' => strtolower($request['password']) ],10000)) {
            sleep(0.2);
            if(strrpos(url()->previous(),"address"))
            {
               return redirect()->back();
            }
            \App\Models\UserLogin::create([
                'user_agent' => \Requests::header('User-Agent'),
                'user_ip' => \Requests::ip(),
                'user_id' => \Auth::id()
                ]);
            $request->session()->forget('faild_login');
            return redirect()->intended('/');
        }else{
            if($request->session()->get('faild_login') > 0 )
            {
                if($request->session()->get('faild_login') > 4 ){
                    sleep(2);                    
                }
                $request->session()->put('faild_login', $request->session()->get('faild_login') + 1);
            }else{
                $request->session()->put('faild_login', 1);
            }
            \Log::error('user cant login with phone: '. $request['phone']);
            
            return redirect()->back()->withErrors('شماره همراه یا رمز عبور اشتباه است.');
        }
    }

    public function getRegister()
    {
        \Meta::set('title','ثبت نام');

        return view('user.register');
    }

    public function postRegister(Request $request)
    {
        \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
            'phone' => 'required|numeric|unique:users|digits:11',
        ])->validate();
        $user = \App\User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'password' => bcrypt( strtolower($request['password']) ),
            // 'password_manager' => $request['password'],
        ]);
        // $user->notify(new \App\Notifications\VerificationCode($user->id));
        $request->session()->flash('alert-success', 'ثبت نام شما با موفقیت انجام شد. شما با شماره همراه '.$request['phone'] .' و رمز عبور '. strtolower($request['password']) .' به سیستم وارد شدید.');
        \Log::info('user register with phone: '. $request['phone']);
        sleep(0.2);
        if (\Auth::attempt(['phone' => $request['phone'], 'password' => strtolower($request['password']) ],
            100000 )) {
            if(strrpos(url()->previous(),"address"))
            {
                $request->session()->forget('alert-success');
                return redirect()->back();
            }
            return redirect()->intended('/');
        } 
    }
}
