@if(Request::segment(1) != 'admin')
<div class="background-footer">
<div class="container">
    <div class="seperate"></div>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-3">
            <h4 class="page-header">
                مرکز نوآوری و کارآفرینی طرفه نگار
            </h4>
        </div>
        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-0 col-md-8 col-lg-9">
            <!-- <img src="/public/img/footer-map.png" class="img-responsive"> -->
            <!-- <div class="seperate"></div> -->
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-4 col-md-3">
            <ul>
                <li>
                <a href="#" >
                    معرفی مرکز 
                </a></li><li>
                <a href="#" >
                    استارتاپ‌ها
                </a></li><li>
                <a href="#" >
                    افراد
                </a></li><li>
                <a href="#" >
                    سوالات متداول
                </a></li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-4 col-md-3">
            <ul>
                <li>
                <a href="#" >
                    معرفی مرکز
                </a></li><li>
                <a href="#" >
                    استارتاپ‌ها
                </a></li><li>
                <a href="#" >
                    افراد
                </a></li><li>
                <a href="#" >
                    سوالات متداول
                </a></li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-4 col-md-3">
            <ul class="list-unstyled">
                <img src="/public/img/logo.png" class="img-responsive" style="max-width: 130px;">
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-md-12 col-md-3 text-right">
            <div>مرکز نوآوری و کارآفرینی طرفه نگار در شبکه‌های اجتماعی</div>
            <div class="half-seperate"></div>
            <div class="social">
                <a href="#">  </a>
                <a href="#">  </a>
                <a href="#">  </a>
                <a href="#">  </a>
                <a href="#">  </a>
                <a href="#">  </a>
            </div>
            <div class="one-third-seperate"></div>
            <p>
            کلیه حقوق مادی و معنوی این اثر متعلق به گروه شرکت های طرفه نگار می باشد 1395-1377


           <!--  کلیه حقوق این سایت متعلق به مرکز نوآوری و کارآفرینی طرفه نگار
            می‌باشد. -->
            </p>
            <!-- <p style="color: #444;font-size: 95%">
                نویسنده سایت :<a href="http://rotbeyek.ir/cv"> طرفه نگار</a>
            </p> -->
        </div>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>
</div>
@endif