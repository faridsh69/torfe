@extends('layout.master-error')
@section('container')
<style type="text/css">
  body{background:url(/public/img/e500.png) 100% 5px no-repeat;}
  @media screen and (max-width:772px){body{background:none;}
</style>
  <p><b>500.</b> <ins> خطایی رخ داده است.</ins>
  <p>اگر می‌‌خواهید به سایت خودتان خدمت کنید ...</p>
  <p>با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس گرفته و خطایی که در زیر می‌بینید را گزارش دهید.</p>
  <p>
    <div style="direction: ltr">
@if(\Config::get('app.debug'))
  <div class="row">
    <div class="col-xs-12">
      {{ $exception->getMessage() }}
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      File: <strong>{{ $exception->getFile() }} - line: {{ $exception->getLine() }}</strong>
    </div>
  </div>
@endif
</div>
<strong>
@endsection