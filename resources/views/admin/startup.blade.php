@extends('admin.dashboard')
@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">
			استارتاپ‌های من:
		</div>
		<div class="panel-body">
		<div class="reponsive-table">
			<table class="table table-striped">
			<thead>
			<tr>
				<th>
				اطلاعات استارتاپ شما 
				</th>
				<!-- <th>
				ویرایش و حذف
				</th> -->
			</tr>
			</thead>
			<tbody>
			@foreach($startups as $startup)
			<tr>
				<td>
					<h4 class="page-header">
						نام استارتاپ: {{ $startup->name }}
					</h4>
					<div class="col-sm-6">
			            <div>
			                <span class="glyphicon glyphicon-blackboard"></span>
			               	نوع درخواست شما از ما: <br>{{ $startup->request }}
			            </div>
			            <div class="seperate"></div>
			            
			             <div>
			                <span class="glyphicon glyphicon-blackboard"></span>
			                تاریخ شروع فعالیت:  <br>{{ $startup->tarikh }}
			            </div>
			            <div class="seperate"></div>
			            <div>
			                <span class="glyphicon glyphicon-blackboard"></span>
			                خلاصه ای از ایده: <br> {{ $startup->kholase }}
			            </div>
			            <div class="seperate"></div>
			            <div>
			                <span class="glyphicon glyphicon-blackboard"></span>
			                آیا استارتاپ شما در مرحله ایده است یا به محصول رسیده اید ؟: <br> {{ $startup->mahsolyaide }}
			            </div>
			        </div>
		    		<div class="col-sm-6">
			            <div>
			                اعضای تیم: <br> {{ $startup->aza }}
			            </div>
			            <div class="seperate"></div>
			            <div>
			                <span class="glyphicon glyphicon-blackboard"></span>
			               	روش آشنایی شما با ما: <br>{{ $startup->ravesh }}
			            </div>
			            <div class="seperate"></div>
			            <div>
			            	<span class="glyphicon glyphicon-blackboard"></span>
			                کسب و کار شما چه ارزش متمایزی را خلق می‌کند ؟ 
			                <br>
			                {{ $startup->sarmaye }} 
			            </div>
		    			<!-- <div>
			                <span class="glyphicon glyphicon-calendar"></span>
			                سال ساخت: {{ $startup->year }}
			            </div>
			            <div>
			                امضا: {{ $startup->vertical ? 'دارد' : 'ندارد' }}
			            </div>
			            <div>
			                وضعیت: {{ $startup->vertical ? 'افقی' : 'عمودی' }}
			            </div>
			           	<div>
			                فریم: {{ $startup->frame ? 'دارد' : 'ندارد' }}
			            </div>
			            <div>
			                {{ $startup->canseen ? '' : 'ن' }}می توانید این نقاشی را از نزدیک ببینید.
			            </div> -->
			        </div>
			        <div class="half-seperate"></div>
			    </div>
				</td>
				
				<!-- <td>
					<a class="btn btn-info btn-sm" href="/admin/startup/edit/{{ $startup->id }}">
					ویرایش
					</a>
					<a class="btn btn-danger btn-sm" href="/admin/art-work/remove/{{ $startup->id }}">
					حذف
					</a>					
				</td> -->
			</tr>
			@endforeach
			</tbody>
			</table>
			</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="post" action="/admin/art-work">
		{{ csrf_field() }}
		<div class="panel panel-default">
		<div class="panel-heading">
			ویرایش اثرات هنری: 
			{{ \Auth::user()->first_name }} 
			{{ \Auth::user()->last_name }}
			<input type="hidden" name="user_id" value="{{ \Auth::user()->id }}">
		</div>
		<div class="panel-body">
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">نام اثر</td>
					<td><input type="text" name="name" class="form-control"
					value="{{  $startup->name  or old('name')  }}" required></td>
				</tr>
				<tr>
					<td>سال ساخت</td>
					<td><input type="text" name="year" class="form-control"
					value="{{ $startup->year or old('year') }}" required>
					</td>
				</tr>
				<tr>
					<td>دسته بندی</td>
					<td>
					<select class="form-control" name="type_id" required>
						@foreach(\App\Models\Type::get() as $type)
						<option value="{{ $type->id }}"
						{{ ( isset($startup) ? $startup->type_id : 1 ) == $type->id ? "selected" : ""}} > 
							{{ $type->name }}
						</option>
						@endforeach
					</select>
					</td>
				</tr>
				<tr>
					<td>قیمت</td>
					<td><input type="number" name="price" class="form-control"
					value="{{ $startup->price or old('price') }}" required></td>
				</tr>
				<tr>
					<td>تکنیک</td>
					<td><input type="text" name="tecnique" class="form-control"
					value="{{ $startup->tecnique or old('tecnique') }}" required></td>
				</tr>
				<tr>
					<td>متریال</td>
					<td><input type="text" name="material" class="form-control"
					value="{{ $startup->material or old('material') }}" required></td>
				</tr>
				<tr>
					<td>سبک</td>
					<td><input type="text" name="sabk" class="form-control"
					value="{{ $startup->sabk or old('sabk') }}" required></td>
				</tr>
				<tr>
					<td>ابعاد</td>
					<td><input type="text" name="size" class="form-control"
					value="{{ $startup->size or old('size') }}" required></td>
				</tr>
				<tr>
					<td>امضا</td>
					<td>
					دارد
					<input type="radio" name="sign" value="1" required 
					checked="{{ $startup->sign or old('sign') == 1 }}">
					ندارد
					<input type="radio" name="sign" value="0" required></td>
				</tr>
				<tr>
					<td>قابلییت رویت</td>
					<td>
					دارد
					<input type="radio" name="canseen" value="1" required
					checked="{{ $startup->sign or old('sign') == 1 }}">
					ندارد
					<input type="radio" name="canseen" value="0" required>
					</td>
				</tr>
				<tr>
					<td>قاب</td>
					<td>
					دارد
					<input type="radio" name="frame" value="1" required
					checked="{{ $startup->sign or old('sign') }}">
					ندارد
					<input type="radio" name="frame" value="0" required>
					</td>
				</tr>
				<tr>
					<td>وضعت</td>
					<td>
					افقی
					<input type="radio" name="vertical" value="1" required
					checked="{{ $startup->sign or old('sign') }}">
					عمودی
					<input type="radio" name="vertical" value="0" required>
					</td>
				</tr>
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر اثر حداکثر ۳۰۰ کلوبایت باشد.</div>
					</td>
					<td>
						<input type='file' accept='image/*' name="startup_image" onchange='uploadImage(event)'>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($startup))
						عکس سابق
						<img src="/storage/art/{{ $startup->id }}-{{ $startup->name}}"
						class="img-responsive img-thumbnail"> 
						@endif
						<p id='no-preview'>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>
				</tr>
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block">ذخیره اطلاعات اثر</button>
        </div>
		</div>

		</form>
	</div>
</div> -->
@endsection
@push('script')
<script>
 	$('#preview').hide();
  	var uploadImage = function(event) {
	    var input = event.target;
	    var reader = new FileReader();
	    reader.onload = function(){
		    $('#preview')[0].src = reader.result;
    	};
	    reader.readAsDataURL(input.files[0]);
	    $('#no-preview').hide();
	    $('#preview').show();
  	};
</script>
@endpush