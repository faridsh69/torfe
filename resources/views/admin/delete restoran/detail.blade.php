<!-- @extends('admin.dashboard')
@section('content')
<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

<select class="selectpicker" data-style="btn-info" multiple data-max-options="3" data-live-search="true">
    <optgroup label="Web">
        <option>PHP</option>
        <option>CSS</option>
        <option>HTML</option>
        <option>CSS 3</option>
        <option>Bootstrap</option>
        <option>JavaScript</option>
    </optgroup>
    <optgroup label="Programming">
      <option>Java</option>
      <option>C#</option>
      <option>Python</option>
    </optgroup>
  </select>
   -->
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="POST">
		{{ csrf_field() }}
		<div class="panel panel-danger">
		<div class="panel-heading">
			ثبت و ویرایش اطلاعات رستوران
		</div>
		<div class="panel-body">
			<button type="submit" class="btn btn-success btn-block"> ذخیره اطلاعات رستوران من </button>
			<div class="seperate"></div>
			<div class="row">
				<div class="col-xs-12">
				@if ($errors->all())
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul class="list-unstyled">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            	@endif
				</div>
			</div>
			@foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
            @endif
            @endforeach
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td width="150">نام رستوران</td>
					<td><input type="text" name="name" class="form-control"
					value="{{ $restaurant->name  or old('name')  }}" required></td>
				</tr>
				<tr>
					<td>شهر</td>
					<td>
					<select class="form-control" name="city_id" required>
						@foreach($cities as $city)
						<option value="{{$city->id}}"  
						{{ ( $restaurant ? $restaurant->city_id : 1 ) == $city->id ? "selected" : ""}} > 
							{{ $city->name }}
						</option>
						@endforeach
					</select></td>
				</tr>
				<tr>
					<td>آدرس</td>
					<td><input type="text" name="address" class="form-control" 
					value="{{ $restaurant->address  or old('address') }}" required></td>
				</tr>
				@can('secretary')
				<tr>
					<td>میزان تخفیف</td>
					<td><input type="number" name="off" class="form-control" min="0" max="99" 
					value="{{ $restaurant->off  or old('off') }}">
					<div class="help-block">اگر می‌خواهید تخفیف نداشته باشید ۰ وارد نمایید.</div>
					</td>
				</tr>
				@endcan	
				<tr>
					<td>مبلغ پیک موتوری</td>
					<td><input type="number" name="peyk_in" class="form-control ltr" min="0" max="4000"	value="{{ $restaurant->peyk_in  or old('peyk_in') }}" required>
					<div class="help-block">با قرار دادن هزینه بالای پیک میزان سفارشات به رستوران شما کم خواهد شد.</div></td>
				</tr>
				<tr>
					<td>حداقل هزینه سفارش از رستوران</td>
					<td><input type="number" name="minimum_price" class="form-control ltr" min="3000" max="15000"
					value="{{ $restaurant->minimum_price  or old('minimum_price') }}" required>
					<div class="help-block">قراردادن رقم حدود ۵-۷ معقول است.</div></td>
					<input type="hidden" name="user_id"	value="{{ $restaurant->user_id or \Auth::id() }}">
				</tr>
				<tr>
					<td>شماره کارت بانک</td>
					<td><input type="text" name="credit_card" class="form-control ltr"
					value="{{ $restaurant->credit_card  or old('credit_card') }}" required>
					<div class="help-block">مثال :  ۱۰۰۰-۲۰۰۰-۴۰۰۰-۸۰۰۰ </div></td>
				</tr>
				<!-- <tr>
					<td>مبلغ پیک موتوری خارج شهر</td>
					<td><input type="text" name="peyk_out" class="form-control ltr"
					value="{{ $restaurant->peyk_out  or old('peyk_out') }}" required>
					<div class="help-block">تا فاصله ۱۰ کیلومتری از شهر.</div></td>
				</tr> -->
				<tr>
					<td>ساعت کاری</td>
					<td>
						<label class="col-xs-3">شروع دریافت سفارش از ساعت</label>
						<input type="number" name="start_time" class="form-control col-xs-3" min="0" max="24"
						value="{{ $restaurant->start_time  or old('start_time') }}" placeholder="12" required>
						<label class="col-xs-1">الی</label>
						<input type="number" name="end_time" class="form-control col-xs-3" min="0" max="24"
						value="{{ $restaurant->end_time  or old('end_time') }}"	placeholder="23" required>
						<div class="half-seperate"></div>
						<div class="help-block">این فیلد بیانگر زمان‌هایی است که مشتری می تواند سفارش دهد.</div>
					</td>
				</tr>		
				<tr>
					<td>شماره همراه</td>
					<td><input type="text" name="phone" class="form-control ltr" 
					value="{{ $restaurant->phone  or old('phone') }}" required>
					<div class="help-block">در تمام ارتباطات با رستوران شما از این شماره استفاده می‌شود.</div></td>
				</tr>
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر غذا حداکثر ۲۰۰ کلوبایت باشد.</div>
					</td>
					<td>
						<input type='file' accept='image/*' name="restaurant_image" onchange='uploadImage(event)'>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($restaurant))
						عکس سابق
						@if($restaurant->image_id)
						<img src="/storage/restaurant/{{$restaurant->image_id}}-{{$restaurant->image->name}}"  class="img-responsive img-thumbnail">
						@else
						 ندارد
						@endif
						@endif
						<p id='no-preview'>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>
				</tr>
				@can('secretary')
				<tr>
					<td>نوع غذاها</td>
					<td>
					<select class="form-control" name="type[]" required multiple style="height:320px">
						@foreach($types as $key => $type)
						<option value="{{$type->id}}" 
						{{array_search( $type->id , $restaurant_types) !== false ? "selected" : ""}} >
							{{ $type->name }}
						</option>
						@endforeach
					</select></td>
				</tr>
				@endcan
			</table>
			</div>
			<button type="submit" class="btn btn-success btn-block"> ذخیره اطلاعات رستوران من </button>
        </div>
		</div>

		</form>
	</div>
</div>
@endsection
@push('script')
<script>
 	$('#preview').hide();
  	var uploadImage = function(event) {
	    var input = event.target;
	    var reader = new FileReader();
	    reader.onload = function(){
		    $('#preview')[0].src = reader.result;
    	};
	    reader.readAsDataURL(input.files[0]);
	    $('#no-preview').hide();
	    $('#preview').show();
  	};
</script>
@endpush -->