@extends('layout.master')
@section('container')
<div class="row">	
	<div class="col-xs-12">
	    <h3 class="text-center">
	    ثبت درخواست از  مرکز نوآوری و کارآفرینی طرفه نگار
	    </h3> 
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>	
<div class="row">
	<div class="col-xs-12">
    <form method="post" action='/register-idea'>
    {{ csrf_field() }}
    <div class="panel panel-default">
    <div class="panel-body">
     	@if(!\Auth::id())
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="seperate"></div>
	        <div class="form-group">
	            <label for="first_name">نام:</label>
	            <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}" required>
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="seperate"></div>
	        <div class="form-group">
	            <label for="last_name">نام خانوادگی:</label>
	            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name')}}" required>
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
 			<div class="form-group">	
	            <label for="phone">شماره همراه:</label>
	            <input type="text" minlength="11" maxlength="11" class="form-control ltr" id="phone" name="phone" value="{{ old('phone') }}" required placeholder="09123456789">
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	 <div class="form-group">
	            <label for="email">ایمیل: <small>(اختیاری)</small></label>
	            <input type="email" class="form-control ltr" id="email" name="email" value="{{ old('email') }}">
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
	            <label for="password">رمز عبور: <small>(رمز عبور محدودیتی ندارد)</small></label>
	            <input type="text" class="form-control ltr" id="password" name="password" required>
	        </div>
	    </div>
	    @endif
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
	            <label for="companyName">نام ایده ، تیم یا شرکت:</label>
	            <input type="text" class="form-control" id="companyName" name="companyName" required value="{{ old('companyName') }}">
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
			    <label for="request">درخواست شما از مرکز نوآوری و کارآفرینی طرفه نگار چیست ؟</label>
				<select id="request" class="form-control" name="request">
					<option value="shetabdehi" selected="">شتابدهی</option>
					<option value="sarmayegozari">سرمایه‌گذاری</option>
					<option value="hamkari">همکاری</option>
					<option value="sayer">سایر</option>
				</select>		    
			</div>
		</div>
		<div class="col-lg-5 col-lg-offset-1">
			<div class="form-group">
	            <label for="tarikh">تاریخ شروع فعالیت در خصوص ایده:</label>
	            <input type="text" class="form-control ltr" id="tarikh" name="tarikh" value="{{ old('tarikh') }}">
	        </div>
		</div>
		<div class="col-lg-5 col-lg-offset-1">
	        <div class="form-group">
	        	<label for="mahsolyaide">آیا استارتاپ شما در مرحله ایده است یا به محصول رسیده اید ؟</label>
	        	<input type="text" class="form-control ltr" id="mahsolyaide" name="mahsolyaide" value="{{ old('mahsolyaide') }}">
	        </div>		
		</div>
		<div class="col-lg-5 col-lg-offset-1">
	         <div class="form-group">
	        	<label for="ravesh">روش آشنایی شما با ما از کجا بوده؟</label>
	        	<input type="text" class="form-control ltr" id="ravesh" name="ravesh" value="{{ old('ravesh') }}">
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
	            <label for="kholase">خلاصه ای از ایده خود و نتایج حاصله را شرح دهید:</label>
	            <textarea class="form-control" id="kholase" rows="4" name="kholase">{{ old('kholase')}}</textarea>
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
	            <label for="last_name">تعداد اعضای تیم و تخصص ها و نقش ها:</label>
	            <textarea class="form-control" rows="4" name="aza">{{ old('aza') }}</textarea>
	        </div>
	    </div>
	    <div class="col-lg-5 col-lg-offset-1">
	    	<div class="form-group">
	            <label for="sarmaye">کسب و کار شما چه ارزش متمایزی را خلق می‌کند ؟</label>
	            <textarea class="form-control" id="sarmaye" rows="4" name="sarmaye">{{ old('sarmaye')}}</textarea>
	        </div>
	    </div>
	    <div class="col-lg-11 col-lg-offset-1 col-md-12 col-md-offset-0">
	    	<div class="seperate"></div>
	        <button type="submit" class="btn btn-primary btn-block">ثبت درخواست</button>
	    </div>
    </div>
    </div>
    </form>
    </div>
</div>
@endsection