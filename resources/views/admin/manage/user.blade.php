@extends('admin.dashboard')
@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-danger">
			<div class="panel-heading">مدیریت کاربران</div>
			<div class="panel-body">
				در این صفحه می‌توان فهرست کاربران را دید و به مدیریت آن‌ها پرداخت.
				<div class="half-seperate"></div>
				<form class="form-inline" method="GET">
				  	<div class="form-group">
				    	<label for="name">نام خانوادگی:</label>
				   	 	<input type="text" class="form-control input-sm" id="name" name="name">
				  	</div>
				  	<div class="form-group">
				    	<label for="phone">تلفن: </label>
				    	<input type="text" class="form-control input-sm" id="phone" name="phone">
				  	</div>
				  	<button type="submit" class="btn btn-default input-sm">جستجو</button>
				</form>
			</div>
			<div class="table-responsive">
			<table class="table table-hover">
			<tr>
				<th width="40">شناسه</th>
				<th>نام و نام‌خانوادگی</th>
				<th>ایمیل</th>
				<th>تلفن</th>
				<th>نقش‌ها</th>
				<th>آدرس ها</th>
				<th>اعتبار</th>
				<th>تاریخ</th>
				<th width="40">لاگین</th>
			</tr>
			@foreach($users as $user)
				<tr>
					<td width="40" class="text-center">{{ $user->id }}</td>
					<td>{{ $user->first_name }}  {{ $user->last_name }}</td>
					<td class="text-left">{{ $user->email }}</td>
					<td>
						<div>{{ \Nopaad\Persian::correct($user->phone) }}</div>
					</td>
					<td style="max-width: 110px">
						<button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#user-roles-modal" data-userid="{{ $user->id }}" data-username="{{ $user->first_name . ' ' . $user->last_name }}">
							+
						</button>
						@foreach($user->roles()->pluck('name') as $role)
							<span class="label label-default">{{ trans('roles.' . $role) }}</span>
						@endforeach
					</td>
					<td style="max-width: 290px">
						@foreach( $user->addresses as $address )
						{{ $address->name }}
						<br>
						@endforeach
					</td>
					<td>
						{{ $user->credit }}
					</td>
					<td>
						{{ \Nopaad\jDate::forge( $user->created_at )->format(' %H:%M:%S') }}
						
						<br>
						{{ \Nopaad\jDate::forge( $user->created_at )->format(' %Y/%m/%d') }}
					</td>
					<td class="text-center">
						<a href="/admin/user/login/{{ $user->id }}">
						<span class="glyphicon glyphicon-log-in">
						</a>
					</td>
				</tr>
			@endforeach
			</table>
			</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center">
				{{ $users->links() }}
			</div>
		</div>
	</div>
	<!-- user roles modal -->
	<div class="modal fade" id="user-roles-modal" tabindex="-1" role="dialog" aria-labelledby="user-roles-modal-label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="user-roles-modal-label">
						افزودن نقش به
					</h4>
				</div>
				<form action="/admin/role" method="POST">
					<div class="modal-body">
						<div class="one-third-separator"><!-- sep --></div>
						<div class="row">
							<div class="col-xs-12">
								{{ csrf_field() }}
								<input id="user-roles-modal-user-id" type="hidden" class="form-control readonly" readonly="readonly" value="userid" name="user_id"/>
								<select class="form-control" name="role_id">
									@foreach(\App\Models\Role::get() as $role)
										<option value="{{ $role->id }}">{{ trans('roles.' . $role->name) }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
						<button type="submit" class="btn btn-primary">ذخیره تغییرات</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal -->
@endsection
@push('script')
<script>
	jQuery('#user-roles-modal').on('show.bs.modal', function (e) {
		jQuery('#user-roles-modal-user-id').val(jQuery(e.relatedTarget).data('userid'));
	})
</script>
@endpush


