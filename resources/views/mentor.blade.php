
@extends('layout.master')
@section('container')
<div class="seperate"></div>
<div class="seperate"></div>
<div class="seperate"></div>
<div class="row">
	<div class="col-xs-12 text-center">
		<h2 class="page-header">
			مربیان: 
		</h2>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<p style="padding: 20px;font-size: 130%;line-height: 150%;text-align: justify;">
مرکز نوآوری و کارآفرینی طرفه نگار یک مجموعه نوآور و کارآفرین در حوزه  شتابدهی و  سرمایه گذاری و بر روی کسب و کارهای نوپا ( استارتاپ ها) می باشد که تا کنون با چندین استارتاپ در حوزه های سرمایه گذاری و خدمات شتاب دهی و مشاوره همکاری داشته است و همواره دست تیم ها و مجموعه های توانمندی که درصدد نیل به اهداف تعالی بخش خود می باشند را به گرمی می فشارد
		</p>
	</div>
</div>
<div class="seperate"></div>
<div class="seperate"></div>

<div class="row">
	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
		<div class="artist text-center">
			<a href="/mentor" class="text-center">
			<div class="artist-img img-circle" 		 
            style="background-image:url(/public/img/a31.png)"  >
			</div>
			<h5 class="artist-title">
				مهندس شکوری
			</h5>
			<div class="one-third-seperate"></div>
			<p class="artist-tecnic">
				
					<span>مربی بخش ... </span>
			</p>
			</a>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
		<div class="artist text-center">
			<a href="/mentor" class="text-center">
			<div class="artist-img img-circle" 		 
            style="background-image:url(/public/img/a32.png)"  >
			</div>
			<h5 class="artist-title">
				حمیدرضا صدیقیان‌راد
			</h5>
			<div class="one-third-seperate"></div>
			<p class="artist-tecnic">
				
					<span>مربی بخش ... </span>
			</p>
			</a>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
		<div class="artist text-center">
			<a href="/mentor" class="text-center">
			<div class="artist-img img-circle" 		 
            style="background-image:url(/public/img/a33.png)"  >
			</div>
			<h5 class="artist-title">
				مهندس اکبر رنجبری
			</h5>
			<div class="one-third-seperate"></div>
			<p class="artist-tecnic">
				
					<span>مربی بخش ... </span>
			</p>
			</a>
		</div>
	</div>


</div>

@endsection

