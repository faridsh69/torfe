@extends('layout.master-error')
@section('container')
<style type="text/css">
  body{background:url(/public/img/403.jpg) 90% -10px no-repeat;}
  @media screen and (max-width:772px){body{background:none;}
</style>
  <p><b>ورود ناموفق.</b> <ins>لطفا مجددا وارد سایت شوید.</ins>
  <p>به دلیل مشکل اینترنت شما این اتفاق افتاده است.</p>
  <p>با تماس با پشتیبانی سایت حتما این مسله را بیان نمایید.</p>
<strong>
@endsection