<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    protected $guarded = [];
    use SoftDeletes;

    public function order()
    {
    	return $this->belongsTo('App\Models\Order');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
