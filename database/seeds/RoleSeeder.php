<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	['id' => 1, 'name' => 'manager'],
            ['id' => 2, 'name' => 'developer'],
            ['id' => 3, 'name' => 'inner_manager'],
        	['id' => 4, 'name' => 'secretary'],
    	];
        foreach($roles as $role)
        {
    	   \App\Models\Role::firstOrCreate($role); 
        }
    }
}
