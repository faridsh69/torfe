<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces =[
        	['id' => 1, 'name' => 'تهران'],
            ['id' => 2, 'name' => 'اصفهان'],
            ['id' => 3, 'name' => 'مازندران'],
    	];
        foreach($provinces as $province)
        {
    	   \App\Models\Province::firstOrCreate($province); 
        }


        $cities =[
            ['name' => 'تهران' , 'province_id'=> 1],
            ['name' => 'کرج' , 'province_id'=> 1],
            ['name' => 'اصفهان' , 'province_id'=> 2],
            ['name' => 'شاهین شهر' , 'province_id'=> 2],
            ['name' => 'شهرضا' , 'province_id'=> 2],
            ['name' => 'خمینی شهر' , 'province_id'=> 2],
            ['name' => 'مبارکه' , 'province_id'=> 2],
            ['name' => 'گلپایگان' , 'province_id'=> 2],
            ['name' => 'فولادشهر' , 'province_id'=> 2],
            ['name' => 'خمین' , 'province_id'=> 2],
            ['name' => 'آمل' , 'province_id'=> 3],
            ['name' => 'ایزد شهر' , 'province_id'=> 3],
            ['name' => 'چمستان' , 'province_id'=> 3],
            ['name' => 'رویان' , 'province_id'=> 3],
            ['name' => 'سرخ رود' , 'province_id'=> 3],
            ['name' => 'فریدون کنار' , 'province_id'=> 3],
            ['name' => 'محموداباد' , 'province_id'=> 3],
            ['name' => 'منطقه لاریجان' , 'province_id'=> 3],
            ['name' => 'نور' , 'province_id'=> 3],
            ['name' => 'بابل' , 'province_id'=> 3],
            ['name' => 'بابلسر' , 'province_id'=> 3],
            ['name' => 'چالوس' , 'province_id'=> 3],
            ['name' => 'ساری' , 'province_id'=> 3],
            ['name' => 'نوشهر' , 'province_id'=> 3],
        ];
        foreach($cities as $city)
        {
           \App\Models\City::firstOrCreate($city); 
        }
    }
}
