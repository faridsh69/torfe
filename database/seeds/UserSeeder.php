<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =[
            ['id' => 1, 'first_name' => 'فرید', 'last_name' => 'شهیدی', 'phone' => '1', 'password' => bcrypt(1)],
            ['id' => 2, 'first_name' => 'آقا', 'last_name' => 'دانش', 'phone' => '2', 'password' => bcrypt(2)]
        ];
        
        foreach($users as $user)
        {
            $user = \App\User::updateOrCreate(['id' => $user['id'] ] , $user);
            if($user->id == 1 ){ 
                $user->roles()->sync([1,2,3,4], true);
            }elseif($user->id == 2 ){
                $user->roles()->sync([1], true);
            }
            elseif($user->id == 3 ){
                $user->roles()->sync([3], true);
            }
        }
    }
}
