<!-- @extends('admin.dashboard')
@section('breadcrumb')
	<ol class="breadcrumb">
		<li>داشبورد</li>
		<li class="active">مدیریت کاربران</li>
	</ol>
@stop
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">پرداختی‌های من</div>
		<div class="table-responsive">
		<table class="table table-hover">
			<tr>
				<th class="text-right" width="40">شناسه</th>
				<th class="text-right">مبلغ</th>
				<th class="text-right">وضعیت</th>
				<th class="text-right">تاریخ</th>
			</tr>
			@if(1 == 2)
			@foreach($payments as $payment)
				<tr>
					<td class="text-center" width="40">{{ $order->id }}</td>
					<td>{{ $order->restaurant->name }}</td>
					<td>
					@foreach( json_decode($order->foods) as $key => $food )
						{{ \Nopaad\Persian::correct( $key+1 ) }}
						{{ \App\Models\Food::where('id', $food)->first()->name }}<br>
					@endforeach
					</td>
					<td>
						{{ \Nopaad\Persian::correct($order->price) }} تومان
					</td>
					<td  style="min-width:140px">
					@if($order->address)
						{{ $order->address->name }}
					@endif	
					</td>
					<td class="text-center" width="100">
						{{ trans('statuses.' . $order->status) }}
					</td>
					<td class="text-center">{{ $order->description }}</td>
				</tr>
			@endforeach
			@endif
		</table>
		</div>
		</div>
	</div>
</div>
@endsection

 -->