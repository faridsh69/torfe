@extends('layout.master')
@section('container')
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12">
        <h3 class="page-header text-center bold">
            مهم‌ترین مطالب مربوط به استارتاپ‌ها
        </h3>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#1">
            شش علامت که نشان می دهد کودک شما در آینده کارآفرین می شود
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="1" class="collapse "> 
            @include('pdf.1')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#2">
            مراحل لین استارتاپ
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="2" class="collapse "> 
            @include('pdf.2')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#3">
            اصل سادگی در نام گذاری و دقیت به آن در نام گذاری استارتاپ
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="3" class="collapse "> 
            @include('pdf.3')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#4">
            استراتژی های قیمت گذاری
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="4" class="collapse "> 
            @include('pdf.4')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#5">
            ریسک، همراه همیشگی استارتاپ‌ها
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="5" class="collapse "> 
            @include('pdf.5')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#6">
            چگونه استارتاپ خود را برای ارائه به سرمایه‌گذار آماده کنید؟
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="6" class="collapse "> 
            @include('pdf.6')
        </div>
    </div>

    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-block" data-toggle="collapse" data-target="#7">
            چه شاخص‌هایی(KPI) برای تولید محتوا در شبکه‌های اجتماعی در نظر بگیریم
            <span class="glyphicon glyphicon-chevron-down "></span>
        </button>
        <div class="seperate"></div>
        <div id="7" class="collapse "> 
            @include('pdf.7')
        </div>
    </div>


</div>
<div class="seperate"></div>
@endsection
