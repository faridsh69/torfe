@extends('layout.master-error')
@section('container')
<style type="text/css">
  body{background:url(/public/img/503.png) 100% 5px no-repeat;}
  @media screen and (max-width:772px){body{background:none;}
</style>
<p><b>503.</b> <ins>سایت در حال به‌روز‌رسانی می‌باشد...</ins>
<p>لطفا چند دقیقه دیگر مراجعه نمایید.</p>
@endsection