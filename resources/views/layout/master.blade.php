<!DOCTYPE html>
<html lang="{{ Lang::locale() }}" dir="{{ Lang::locale() == 'fa' ? 'rtl' : 'ltr' }}">
	<head>
		@include('common.header')
	</head>
	<body>
		@include('common.navbar')
		<div id="body_id">
			<div class="container-fluid background-container-fluid">
				@yield('fluid-container')
			</div>
			<div class="container">
				<div class="background-container">
					@yield('container')
				</div>
			</div>
		</div>
		@include('common.footer')
		@include('common.script')
		@stack('script')
	</body>
</html>